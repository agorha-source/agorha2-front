// Lors du build, le notifier dans la console
if (process.env.npm_lifecycle_event === "build") {
  console.info("\x1B[31m", `* Building...`, "\x1B[0m");
}
// Chargement du fichier de variables d'environnement en fonction de
// l'argument env passé dans la commande "npm run front --env=..."
// Voir package.json pour le détail
let environment = process.env.npm_config_env;
if (!environment) { environment = "local"; }
const ENV_VARS = require(`./env/${environment}.json`);
// Affichage de l'environnement
if (process.env.npm_lifecycle_event !== "build") {
  console.info("\x1B[45m", `Environment: ${ENV_VARS.NODE_ENV}`, "\x1B[0m");
}

export default {
  /** @deprecated */
  env: {
    defaultLanguage: ENV_VARS.DEFAULT_LANGUAGE || "fre"
  },
  loading: {
    color: "#72b8ae",
    height: "5px",
    continuous: true,
    throttle: 10
  },
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ]
    // L'icône est gérée par le CMS englobant Typo3
    // link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  /*
   ** Global CSS
   */
  css: [
    "@/assets/fonts/lato.css",
    "@/assets/scss/bootstrap_override.scss",
    "@fortawesome/fontawesome-svg-core/styles.css",
    "@/assets/scss/main.scss"
  ],
  styleResources: {
    scss: [
      "bootstrap/scss/_functions.scss",
      "bootstrap/scss/_variables.scss",
      "bootstrap/scss/_mixins.scss",
      "bootstrap-vue/src/_variables.scss",
      "@/assets/scss/agorha_variables.scss"
    ]
  },
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    { src: "@/plugins/axios" },
    { src: "@/plugins/fontawesome" },
    { src: "@/plugins/facets" },
    { src: "@/plugins/vue-json-viewer", ssr: true },
    { src: "@/plugins/clipboard", ssr: false },
    { src: "@/plugins/vue-window-size", ssr: false },
    { src: "@/plugins/vee-validate", ssr: false },
    { src: "@/plugins/vue-infinite-loading", ssr: false },
    { src: "@/plugins/mirador", ssr: false },
    { src: "@/plugins/leaflet", ssr: false },
    { src: "@/plugins/liquor-tree", ssr: false },
    { src: "@/plugins/timeline", ssr: false },
    { src: "@/plugins/jsonpath-faster", mode: "client" },
    { src: "@/plugins/rich-text-editor", mode: "client" },
    { src: "@/plugins/v-hotkey", mode: "client" },
    { src: "@/plugins/event-matomo-push", mode: "client" },
    { src: "@/plugins/vuedraggable.js", ssr: false }
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,

  /**
   * Axios module configuration
   * See https://axios.nuxtjs.org/options
   */
  axios: {
    credentials: true,
    baseURL: ENV_VARS.BASE_API_URL,
    prefix: ENV_VARS.BASE_API_URL
  },
  publicRuntimeConfig: {
    axios: {
      browserBaseURL: ENV_VARS.BASE_API_URL
    },
    defaultLanguage: ENV_VARS.DEFAULT_LANGUAGE || "fre",
    manifestUrl:
      ENV_VARS.MANIFEST_BASE_URl || "http://localhost/iiif/presentation/v2/",
    canonicalUrl: ENV_VARS.CANONICAL_URL || "http://localhost",
    // Pour les appels au thesaurus
    thesaurusListLimit: 10,
    thesaurusPrefixDateList: "TH_PREFIXE",
    thesaurusSiecleAutoComp: "TH_SIECLES",
    thesaurusLanguagesAutoComp: "LANGUE_ISO_ABR",
    rmnApiKey: ENV_VARS.RMN_API_KEY,
    rmnApiUrl: ENV_VARS.RMN_API_URL
  },
  privateRuntimeConfig: {
    axios: {
      baseURL: ENV_VARS.BASE_API_URL
    },
    rmnApiKey: ENV_VARS.RMN_API_KEY,
    rmnApiUrl: ENV_VARS.RMN_API_URL
  },

  /*
   ** Router configuration
   ** See https://nuxtjs.org/api/configuration-router
   */
  router: {
    extendRoutes: (routesIn) => {
      routesIn.forEach((r) => {
        if (r.path.includes("notice")) {
          r.path = r.path.replace("notice", "ark:/54721");
        }
      });
      return routesIn;
    }
  },
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    "@nuxtjs/style-resources",
    "nuxt-compress",
    "@nuxtjs/moment"
  ],
  moment: {
    defaultLocale: "fr",
    locales: ["fr"]
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    "bootstrap-vue/nuxt",
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/auth",
    "nuxt-svg-loader",
    "nuxt-compress"
  ],
  "nuxt-compress": {
    gzip: {
      cache: true
    },
    brotli: {
      threshold: 10240
    }
  },
  // Configuration de l'authentification
  auth: {
    // plugins: [{ src: "@/plugins/axios", ssr: true }, "@/plugins/auth.js"],
    localStorage: false,
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "users/isAuthenticated",
            method: "get",
            propertyName: false,
            withCredentials: true // obligatoire
          },
          user: {
            url: "users/current",
            method: "get",
            propertyName: false,
            withCredentials: true // obligatoire
          },
          logout: false
        },
        tokenRequired: false,
        tokenType: false
      }
    }
  },

  // Doc: https://bootstrap-vue.org/docs#using-custom-bootstrap-scss
  bootstrapVue: {
    icons: false,
    bootstrapCSS: false,
    bootstrapVueCSS: false
  },

  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    // analyze: true, // seulement en dev
    optimizeCSS: true,
    // ---
    // Pour éviter l'erreur : Unexpected token 'export'
    // on compile avec babel les modules suivants
    transpile: [
      "getty-entity-lookup",
      "dbpedia-entity-lookup",
      "wikidata-entity-lookup",
      "geonames-entity-lookup"
    ],
    babel: {
      cacheDirectory: true,
      compact: true
    },
    optimization: {
      splitChunks: {
        chunks: "all",
        automaticNameDelimiter: ".",
        name: "inha",
        // minSize: 16000,
        // maxSize: 24000,
        cacheGroups: {
          styles: {
            name: "styles",
            test: /\.(css|vue)$/,
            chunks: "all",
            enforce: true
          }
        }
      }
    }
  }
};
