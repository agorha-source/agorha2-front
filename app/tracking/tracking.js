export * from "./pageNotice";

export function createInteraction(category, action, label, value) {
  let data = label ? {
    event: "interaction",
    category,
    action,
    label
  } : {
    event: "interaction",
    category,
    action
  };
  if (value) {
    data = {
      event: "interaction",
      category,
      action,
      label,
      value
    };
  }
  if (typeof (window._mtm) !== "undefined") {
    window._mtm.push(data);
  }
}

export function searchPageTracking(currentSearch, searchCat, resultsNumber) {
  if (typeof (window._mtm) !== "undefined") {
    window._mtm.push({
      event: "page",
      page: {
        page404: "N",
        pageType: "Search",
        siteSection: "Search",
        searchKeywords: currentSearch,
        searchCategory: searchCat,
        searchResultsNumber: resultsNumber
      }
    });
  }
}
