export function pageNoticeTracking(noticeName, noticeType, noticeDatabases) {
  if (typeof (window._mtm) !== "undefined") {
    window._mtm.push({
      event: "page",
      page: {
        page404: "N",
        pageType: "Notices",
        siteSection: "Notices",
        recordType: noticeType,
        recordRelatedProgram: noticeDatabases,
        recordName: noticeName
      }
    });
  }
}
