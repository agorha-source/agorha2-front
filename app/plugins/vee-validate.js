import Vue from "vue";
import VeeValidate, { Validator, ValidationProvider } from "vee-validate";

import fr from "vee-validate/dist/locale/fr";

Validator.localize("fr", {
  messages: fr.messages,
  attributes: {
    required: fr.attributes
  }
});

Vue.component("ValidationProvider", ValidationProvider);

Vue.use(VeeValidate, {
  locale: "fr",
  inject: true,
  fieldsBagName: "veeFields",
  errorBagName: "veeErrors"
});
