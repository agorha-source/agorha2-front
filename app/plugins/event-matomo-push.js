import * as dataLayer from "@/tracking/tracking";

export default (context, inject) => {
  inject("dataLayer", dataLayer);
};
