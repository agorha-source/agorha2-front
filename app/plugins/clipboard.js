export default (context, inject) => {
  const clipboard = (target) => {
    const dummy = document.createElement("input");
    document.body.appendChild(dummy);
    dummy.setAttribute("id", "dummy_id");
    document.getElementById("dummy_id").value = target;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
  };
  // Inject in Vue, context and store.
  inject("clipboard", clipboard);
  // For Nuxt <= 2.12
  context.$clipboard = clipboard;
};
