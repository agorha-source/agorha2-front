import Mirador from "mirador";
import { miradorImageToolsPlugin } from "mirador-image-tools";

/** Instanciation de la visionneuse */
function loadViewer(config) {
  return Mirador.viewer(config, [...miradorImageToolsPlugin]);
}
/** Récupération de la configuration de la visionneuse */
function getConfig(targetId, manifestFile, canvasId = undefined) {
  // Voir https://github.com/ProjectMirador/mirador/blob/master/src/config/settings.js
  // pour un exemple de configuration
  const config = {
    id: targetId,
    canvasNavigation: {
      // Set the hight and width of canvas thumbnails in the  CanvasNavigation companion window
      height: 50,
      width: 50
    },
    selectedTheme: "light", // dark also available
    theme: {
      // Sets up a MaterialUI theme. See https://material-ui.com/customization/default-theme/
      typography: {
        subtitle2: {
          fontWeight: 1500
        },
        useNextVariants: true // set so that console deprecation warning is removed
      }
    },
    language: "fr", // The default language set in the application
    classPrefix: "mirador",
    window: {
      // Global window defaults
      allowClose: false, // Configure if windows can be closed or not
      allowFullscreen: true, // Configure to show a "fullscreen" button in the WindowTopBar
      allowMaximize: true, // Configure if windows can be maximized or not
      allowTopMenuButton: true, // Configure if window view and thumbnail display menu are visible or not
      allowWindowSideBar: true, // Configure if side bar menu is visible or not
      authNewWindowCenter: "parent", // Configure how to center a new window created by the authentication flow. Options: parent, screen
      sideBarPanel: "info", // Configure which sidebar is selected by default. Options: info, attribution, canvas, annotations, search
      defaultSidebarPanelHeight: 201, // Configure default sidebar height in pixels
      defaultSidebarPanelWidth: 235, // Configure default sidebar width in pixels
      defaultView: "single", // Configure which viewing mode (e.g. single, book, gallery) for windows to be opened in
      forceDrawAnnotations: false,
      hideWindowTitle: false, // Configure if the window title is shown in the window title bar or not
      highlightAllAnnotations: false, // Configure whether to display annotations on the canvas by default
      showLocalePicker: false, // Configure locale picker for multi-lingual metadata
      sideBarOpen: false, // Configure if the sidebar (and its content panel) is open by default
      panels: {
        // Configure which panels are visible in WindowSideBarButtons
        info: true,
        attribution: true,
        canvas: true,
        annotations: false,
        search: false
      }
    },
    windows: [
      {
        id: "inha-window", // Pour y accéder depuis MediaViewer.vue
        manifestId: manifestFile, // Lien vers le manifeste
        canvasId, // URL du canvas (équivalent de page)
        canvasIndex: undefined, // DEPRECATED position dans le canvas
        imageToolsEnabled: true,
        imageToolsOpen: false,
        thumbnailNavigationPosition: "far-bottom"
      }
    ],
    workspace: {
      draggingEnabled: true,
      allowNewWindows: false,
      exposeModeOn: false, // unused?
      showZoomControls: true, // Configure if zoom controls should be displayed by default
      type: "mosaic", // Which workspace type to load by default. Other possible values are "elastic"
      width: 5000 // width of the elastic mode's virtual canvas
    },
    osdConfig: {
      // Default config used for OpenSeadragon
      // alwaysBlend: false,
      // blendTime: 0.1,
      preserveImageSizeOnResize: false // Evite le bug du dézoom initial
      // preserveViewport: false,
      // showNavigationControl: false
    },
    workspaceControlPanel: {
      enabled: false // Configure if the control panel should be rendered.  Useful if you want to lock the viewer down to only the configured manifests
    }
  };
  return config;
}

export default (context, inject) => {
  inject("mirador", Mirador);
  inject("miradorImageToolsPlugin", miradorImageToolsPlugin);
  inject("miradorConfig", getConfig);
  inject("miradorInstance", loadViewer);
};
