import Vue from "vue";
import RichTextEditor from "@/components/util/RichTextEditor";

Vue.component("rte", RichTextEditor);
