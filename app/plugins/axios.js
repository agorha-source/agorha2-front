export default function({ store, app: { $axios, redirect } }) {
  $axios.onRequest((config) => {
    if (should_display_loading(config) && !store.getters["loader/getApiLoading"]) {
      store.dispatch("loader/startLoading", config.url);
    } else if (store.getters["loader/getApiLoading"]) { store.dispatch("loader/finishLoading"); }
  });
  $axios.onResponse((response) => {
    if (store.getters["loader/getApiLoading"]) { store.dispatch("loader/finishLoading"); }
    return response;
  });
  $axios.onError((error) => {
    if (store.getters["loader/getApiLoading"]) { store.dispatch("loader/finishLoading"); }
    return Promise.reject(error);
  });
}

/**
 * N'affichent pas l'écran de chargement :
 * - Télécharger des images
 * - Activer l'autocomplétion
 * - Intéractions avec les sélections
 */
function should_display_loading(config) {
  const { url } = config;
  const notLoading = ["autocomplete", "advAutocomplete", "selections"];
  if (!url) { return false; }
  // Si l'url contient un des endpoints listés, ne pas afficher la page de chargement
  return !notLoading.some(x => url.toLowerCase().includes(x.toLowerCase()));
}
