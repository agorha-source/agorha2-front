// FIXME: inutilisé pour le moment
export default function({ $auth }) {
  $auth.onError((error, name, endpoint) => {
    const code = parseInt(error.response && error.response.status);
    if (code === 401 && endpoint.includes("users/current")) {
      console.warn("Utilisateur non connecté");
    } else {
      console.error(name, error);
    }
    console.error("auth ?");
  });
}
