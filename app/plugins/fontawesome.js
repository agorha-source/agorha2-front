import Vue from "vue";
import { library, config } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import { faPlusSquare } from "@fortawesome/free-solid-svg-icons/faPlusSquare";
import { faMinusSquare } from "@fortawesome/free-solid-svg-icons/faMinusSquare";
import { faFilter } from "@fortawesome/free-solid-svg-icons/faFilter";
import { faSave } from "@fortawesome/free-solid-svg-icons/faSave";
import { faSlidersH } from "@fortawesome/free-solid-svg-icons/faSlidersH";
import { faThList } from "@fortawesome/free-solid-svg-icons/faThList";
import { faTh } from "@fortawesome/free-solid-svg-icons/faTh";
import { faAlignJustify } from "@fortawesome/free-solid-svg-icons/faAlignJustify";
import { faChartPie } from "@fortawesome/free-solid-svg-icons/faChartPie";
import { faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons/faMapMarkerAlt";
import { faListOl } from "@fortawesome/free-solid-svg-icons/faListOl";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons/faPencilAlt";
import { faCopy } from "@fortawesome/free-solid-svg-icons/faCopy";
import { faTrash } from "@fortawesome/free-solid-svg-icons/faTrash";
import { faTimes } from "@fortawesome/free-solid-svg-icons/faTimes";
import { faSpellCheck } from "@fortawesome/free-solid-svg-icons/faSpellCheck";
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons/faTimesCircle";
import { faClipboard } from "@fortawesome/free-solid-svg-icons/faClipboard";
import { faStream } from "@fortawesome/free-solid-svg-icons/faStream";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons/faInfoCircle";
import { faCalendarAlt } from "@fortawesome/free-regular-svg-icons/faCalendarAlt";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons/faCaretDown";

// This is important, we are going to let Nuxt.js worry about the CSS
config.autoAddCss = false;

// You can add your icons directly in this plugin. See other examples for how you
// can add other styles or just individual icons.
library.add(faPlusSquare, faMinusSquare, faFilter, faSave,
  faSlidersH, faThList, faTh, faAlignJustify, faChartPie,
  faMapMarkerAlt, faListOl, faPencilAlt, faCopy, faTrash,
  faTimes, faSpellCheck, faTimesCircle, faClipboard, faStream,
  faInfoCircle, faCalendarAlt, faCaretDown);
// Register the component globally
Vue.component("font-awesome-icon", FontAwesomeIcon);
