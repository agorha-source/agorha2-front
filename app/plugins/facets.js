import atob from "atob";

/**
 * Décode une chaîne de caractères donnée en base64
 * @param {string} str la chaîne de caractère encodée en base64
 */
function b64DecodeUnicode(str) {
  // From bytestream, to percent-encoding, to original string.
  return decodeURIComponent(
    atob(str)
      .split("")
      .map(function(c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );
}

/**
 * Sérialise en base64 un objet de facettes
 * ou de filtres (dans le cas d'une recherche avancée)
 * ET encode la chaîne pour manipulation en tant qu'URI.
 *
 * @param {Object} f facets ou filters
 */
export const encodeFacets = f =>
  Buffer.from(JSON.stringify(f)).toString("base64");

/**
 * Désérialise une chaîne représentant des facettes
 * ou des filtres (dans le cas d'une recherche avancée)
 * provenant d'une URL.
 *
 * @param {String} text URL encodée + base64
 */
export const decodeFacets = text =>
  JSON.parse(b64DecodeUnicode(text));

export default (context, inject) => {
  inject("encodeFacets", encodeFacets);
  inject("decodeFacets", decodeFacets);
  context.$encodeFacets = encodeFacets;
  context.$decodeFacets = decodeFacets;
};
