export default (context, inject) => {
  const jp = require("jsonpath-faster");
  inject("jp", jp);
  context.$jp = jp;
};
