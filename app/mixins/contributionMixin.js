export default {
  methods: {
    /**
     * Extraction du libellé localisé d'un prefLabel,
     * depuis une liste de prefLabels.
     * (i) Locale par défaut: 'fre'.
     */
    localizedPrefLabel(prefLabels) {
      if (!prefLabels) { return ""; }
      const locale = this.$config.defaultLanguage ?? "fre";
      const value = prefLabels.find(x => x.language === locale);
      return value ?? "";
    },
    /**
       * Extration d'un préfixe au format accepté par le formulaire.
       * En entrée : prefix.thesaurus: { prefLabels: Array, ref: String }
       */
    extractPrefix(prefix) {
      if (prefix) {
        const formatted = {
          id: prefix?.thesaurus.ref,
          libelle: this.localizedPrefLabel(prefix.thesaurus.prefLabels).value
        };
        return formatted;
      }
      return {};
    },
    /**
     * Formater une date provenant d'une notice
     * pour réinjection dans le formulaire de modification d'une notice.
     * (i) Fonction inverse de InputDate.vue -> transformDate */
    dateFormAdapter(date) {
      const adaptedDate = {
        selectFormat: "",
        selectPrefix: {},
        siecle: {
          identifier: "",
          lexicalValue: ""
        },
        annee: "",
        anneeBis: "",
        // active: false, à rajouter pour une dateFin
        interval: false
      };
      const { siecle, earliest, latest, prefix } = date;
      // Prefix
      adaptedDate.selectPrefix = this.extractPrefix(prefix);
      // Format "Siècle"
      if (siecle) {
        // Date type
        adaptedDate.selectFormat = "siecle";
        // Lexical value
        const prefLabel = this.localizedPrefLabel(siecle.thesaurus?.prefLabels);
        const ref = siecle.thesaurus?.ref;
        if (prefLabel && Object.values(prefLabel)) {
          adaptedDate.siecle.identifier = ref;
          adaptedDate.siecle.lexicalValue = prefLabel.value;
        }
        // Format "Date"
      } else {
        // Date type
        adaptedDate.selectFormat = "date";
        if (earliest) {
          adaptedDate.annee = earliest.date;
        }
        if (latest) {
          adaptedDate.interval = true;
          adaptedDate.anneeBis = latest.date;
        }
      }
      return adaptedDate;
    }
  }
};
