import moment from "moment";

// TODO: refactor et déplacer dans filterHelper.js ?
export default {
  methods: {
    /**
     * Affiche de façon lisible la valeur d'un filtre
     * @param {string} filterValue le filtre à afficher
     */
    dispayFilterValue(filterValue) {
      if (typeof filterValue === "object") {
        // TODO: attention, vérifier si les autres cas sont gérés correctement
        if (filterValue.value !== "") { return filterValue.value; }
        // C'est une date ou un numérique
        if (filterValue.min && filterValue.max) {
          // Format différent selon si c'est un nombre ou une date
          const formatedValueMin =
            !!Number(filterValue.min) || Number(filterValue.min) === 0
              ? filterValue.min
              : this.formatFilterDate(filterValue.min);
          const formatedValueMax =
            !!Number(filterValue.max) || Number(filterValue.max) === 0
              ? filterValue.max
              : this.formatFilterDate(filterValue.max);

          if (filterValue.min === filterValue.max) {
            return formatedValueMin;
          } else {
            return formatedValueMin + " - " + formatedValueMax;
          }
        } else if (filterValue.min) {
          const formatedValueMin =
            !!Number(filterValue.min) || Number(filterValue.min) === 0
              ? filterValue.min
              : this.formatFilterDate(filterValue.min);
          return "> " + formatedValueMin;
        } else if (filterValue.max) {
          const formatedValueMax =
            !!Number(filterValue.max) || Number(filterValue.max) === 0
              ? filterValue.max
              : this.formatFilterDate(filterValue.max);
          return "< " + formatedValueMax;
        }
      } else {
        // C'est une simple valeur textuelle
        return filterValue;
      }
    },
    /**
     * Formate la date pour la rendre affichable dans l'historique même si elle est avant JC
     * @param {string} date la date à formater
     */
    formatFilterDate(date) {
      let formatedDate;
      let baseFormat;
      let targetFormat;
      const dateTab = date.split("-");
      if (dateTab?.length === 1) {
        baseFormat = "Y";
        targetFormat = "Y";
      } else if (dateTab?.length === 2) {
        baseFormat = "Y-MM";
        targetFormat = "MM/Y";
      } else if (dateTab?.length === 3) {
        baseFormat = "Y-MM-DD";
        targetFormat = "DD/MM/Y";
      }

      moment.locale("fr");
      if (!date) {
        return null;
      } else if (date.includes(" BC")) {
        formatedDate = moment(date, baseFormat);
        formatedDate = formatedDate.format(targetFormat) + " avant notre ère";
        return formatedDate.replace("-", "");
      } else {
        formatedDate = moment(date, baseFormat);
        return formatedDate.format(targetFormat);
      }
    }
  }
};
