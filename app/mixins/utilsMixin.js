import { _formatDate } from "@/helpers/dateHelper";
import moment from "moment";
import { associationCouleurEnum, typesEnum } from "@/const/_notice";

export default {
  computed: {
    authentified() {
      return (
        this.$auth.loggedIn &&
        (this.$auth?.user.authorizations?.role === "ADMIN" ||
          this.$auth?.user.authorizations?.role === "CONTRIB" ||
          this.$auth?.user.authorizations?.role === "DATABASE_MANAGER")
      );
    }
  },
  filters: {
    truncate(value, n) {
      if (typeof value === "string") {
        return value.length > n ? value.substr(0, n) + " [...]" : value;
      } else {
        return "Donnée invalide";
      }
    }
  },
  methods: {
    /** URL de recherche d'un concept */
    conceptUrl(ref) {
      const concept = ref.substring(ref.lastIndexOf("/") + 1) ?? "";
      const otherParams = "page=1&pageSize=20";
      if (ref) {
        return `/recherche?terms=concept_lie: "${concept}"&${otherParams}`;
      } else {
        console.error("Concept non trouvé");
        return "#";
      }
    },
    /** URL de recherche d'une référence */
    referenceUrl(ref) {
      const reference = ref.substring(ref.lastIndexOf("/") + 1) ?? "";
      const otherParams = "page=1&pageSize=20";
      if (ref) {
        return `/recherche?terms=notice_liee: "${reference}"&${otherParams}`;
      } else {
        console.error("Référence non trouvée");
        return "#";
      }
    },
    /**
     * Extraction de l'URL de la miniature d'un résultat
     * de recherche, si elle existe.
     */
    getThumbnail(result) {
      return result?.content?.mediaInformation?.prefPicture.thumbnail ?? "";
    },
    /**
     * Convertit le libellé technique d'un type de notice
     * en libellé affichable.
     * Exemple: "ARTWORK" -> "Oeuvre"
     * - Voir : "@/const/_notices/typesEnum"
     * @param {String} noticeType
     */
    getLabel(noticeType) {
      return typesEnum[noticeType] ?? "Inconnu";
    },
    /**
     * @deprecated : prévoir la suppression de l'utilisation de la locale
     * Récupération de la traduction d'un champ en
     * fonction de la langue courante. Si aucune langue n'est
     * paramétrée, le champ 'default' est choisi
     * @param {Object} text text à traduire au format {lang1: "value1", langue2:"value2", ...}
     */
    getTraduction(text) {
      if (text) {
        if (text[this.$config.defaultLanguage]) {
          return text[this.$config.defaultLanguage];
        } else if (text.default) {
          return text.default;
        } else if (text[0] && text[0].value) {
          return text[0].value;
        } else if (text.value) {
          return text.value;
        } else if (typeof text === "string") {
          return text;
        }
      }
      return "";
    },

    /**
     * Devrait remplacer la méthode getTraduction(String).
     * Récupère une valeur texte dans le prototype de l'objet,
     * jusqu'à obtenir une valeur affichable.
     * (i) L'utilisation d'une locale est parfois nécessaire
     * pour accéder à la valeur textuelle d'un champ.
     */
    getTextValue(text) {
      if (text) {
        const locale = this.$config.defaultLanguage;
        if (typeof text === "string") {
          return text;
        }
        if (text[locale]) {
          return text[locale];
        }
        if (text.default) {
          return text.default;
        }
        if (text?.[0]?.value) {
          return text?.[0]?.value;
        }
        if (text.value) {
          return text.value;
        }
      } else {
        return "Non renseigné";
      }
    },

    // TODO: refactor pour ne pas utiliser de balises HTML
    /** Forme le digest d'autocomplétion adapté pour chaque type de notice */
    getDigest(digest) {
      let digestFull = "";
      if (digest.noticeType === "COLLECTION") {
        // Digest COLLECTION
        digestFull = "<span><b>";
        if (digest.title) {
          digestFull += digest.title;
        }
        digestFull += "</b></span>";
        if (digest.startDate || digest.endDate) {
          digestFull += ", " + this.getDate(digest.startDate, digest.endDate);
        }
      } else if (digest.noticeType === "EVENT") {
        // Digest EVENT
        digestFull += "<span><b>";
        if (digest.title) {
          digestFull += digest.title;
        }
        digestFull += "</b></span>";
        if (digest.startDate || digest.endDate) {
          digestFull += ", " + this.getDate(digest.startDate, digest.endDate);
        }

        if (digest.place) {
          digestFull += ", " + digest.place;
        }
      } else if (digest.noticeType === "ARTWORK") {
        // Digest ARTWORK
        digestFull = "<span><b>";
        if (digest.title) {
          digestFull += digest.title;
        }
        digestFull += "</b></span>";
        if (digest.usualFirstName || digest.usualName) {
          digestFull +=
            ", " + this.getDigestName(digest.usualFirstName, digest.usualName);
          if (digest.authorRole) {
            digestFull += ", " + digest.authorRole;
          }
        }
        if (digest.startDate || digest.endDate) {
          digestFull +=
            " (" + this.getDate(digest.startDate, digest.endDate) + ")";
        }
        if (digest.activityType) {
          digestFull += ", " + digest.activityType;
        }
        if (digest.datationExecutionStart || digest.datationExecutionEnd) {
          digestFull +=
            ", " +
            this.getDate(
              digest.datationExecutionStart,
              digest.datationExecutionEnd
            );
        }
        if (digest.place) {
          digestFull += ", " + digest.place;
        }
        if (digest.shelfMark) {
          digestFull += ", " + digest.shelfMark;
        }
        if (digest.shelfMarkType) {
          digestFull += ", " + digest.shelfMarkType;
        }
      } else if (digest.noticeType === "PERSON") {
        // Digest PERSON
        digestFull = "<span><b>";
        if (digest.title) {
          digestFull += digest.title;
        }
        digestFull += "</b></span>";
        if (digest.startDate || digest.endDate) {
          digestFull += ", " + this.getDate(digest.startDate, digest.endDate);
        }
        if (digest.place) {
          digestFull += ", " + digest.place;
        }
        if (digest.activityType) {
          digestFull += ", " + digest.activityType;
        }
      } else if (digest.noticeType === "REFERENCE") {
        // Digest REFERENCE
        digestFull = "<span><b>";
        if (digest.title) {
          digestFull += digest.title;
        }
        digestFull += "</b></span>";
        if (digest.journalTitle) {
          digestFull += ", " + digest.journalTitle;
        }
        if (digest.authorName) {
          digestFull += ", " + digest.authorName;
        }
        if (digest.editionPlace) {
          digestFull += ", " + digest.editionPlace;
        }
        if (digest.editor) {
          if (digest.editionPlace) {
            digestFull += " : ";
          } else {
            digestFull += ", ";
          }
          digestFull += digest.editor;
        }
        if (digest.startDate || digest.endDate) {
          digestFull += ", " + this.getDate(digest.startDate, digest.endDate);
        }
        if (digest.institution) {
          digestFull += ", " + digest.institution;
        }
        if (digest.institution) {
          digestFull += ", " + digest.shelfMark;
        }
      } else {
        return "Erreur, ce type de notice n'existe pas.";
      }
      return digestFull;
    },

    /**
     * Forme la chaîne de caractère contenant un nom
     * au format "NOM PRENOM"
     * @param {string} firstName le nom
     * @param {string} name le prénom
     */
    getDigestName(firstName, name) {
      let fullname = "";
      if (firstName || name) {
        fullname = `${firstName ?? ""} ${name ?? ""}`.trim();
      }
      return fullname;
    },

    /**
     * Génère la chaîne de caractère formatée d'une date en tenant
     * compte du format de date. Le format final est de la forme
     * start.prefix start.start / start.end - end.prefix end.start / end.end
     * @param {Object} start date de début formatée comme dans le schéma
     * @param {Object} end date de fin formatée comme dans le schéma
     */
    getDate(start, end) {
      if (start || end) {
        let date = "";
        let computedStart = "";
        let computedEnd = "";
        if (start) {
          // Formatage de la date de début
          if (start.prefix) {
            computedStart = start.prefix + " ";
          }
          if (start.siecle) {
            computedStart += this.getTraduction(
              start.siecle.thesaurus.prefLabels
            );
          } else {
            if (start.earliest && start.earliestFormat) {
              computedStart += this.formatDateFormat(
                start.earliest,
                start.earliestFormat
              );
            } else if (start.earliest) {
              computedStart += start.earliest;
            }
            if (start.latest && start.latestFormat) {
              computedStart +=
                " / " + this.formatDateFormat(start.latest, start.latestFormat);
            } else if (start.latest) {
              computedStart += start.latest;
            }
          }
        }

        if (end) {
          // Formatage de la date de fin
          if (end.prefix) {
            computedEnd += end.prefix + " ";
          }
          if (end.siecle) {
            computedEnd += this.getTraduction(end.siecle.thesaurus.prefLabels);
          } else {
            if (end.earliest && end.earliestFormat) {
              computedEnd += this.formatDateFormat(
                end.earliest,
                end.earliestFormat
              );
            } else if (end.earliest) {
              computedEnd += end.earliest;
            }
            if (end.latest && end.latestFormat) {
              computedEnd +=
                " / " + this.formatDateFormat(end.latest, end.latestFormat);
            } else if (end.latest) {
              computedEnd += end.latest;
            }
          }
        }

        if (computedStart === computedEnd) {
          return computedStart;
        } else {
          date = computedStart;
          if (computedEnd) {
            date += " - " + computedEnd;
          }
        }
        return date;
      }
      return null;
    },
    /**
     * @deprecated : réécrire cette méthode sans moment.js
     * Formatage d'une date en fonction d'un format désiré. Une date
     * avant JC sera au format : "dateFormatée avant notre ère"
     * @param {string} date la date à formater
     * @param {string} format le format désiré
     */
    formatDateFormat(date, format) {
      // Préparation du traitement des valeurs
      let formatedDate;
      // Sans format fourni, on renvoie la date telle quelle
      if (!format) {
        return date;
      }
      format = format.toUpperCase();
      const chars = { "/": " ", DD: "D", MM: "MMMM", YYYY: "Y" };

      // Reconnaissance du format de la date reçue
      let baseFormat;
      const dateTab = date.split("/");
      if (dateTab.length === 1) {
        baseFormat = "Y";
      } else if (dateTab.length === 2) {
        baseFormat = "MM/Y";
      } else if (dateTab.length === 3) {
        baseFormat = "DD/MM/Y";
      }

      // Formattage de la date
      if (format) {
        moment.locale("fr");
        if (!date) {
          return null;
        } else if (date.includes("-")) {
          let positivDate = date.replace("-", "");
          formatedDate = moment(positivDate, baseFormat);
          positivDate = formatedDate.format(
            format.replace(/\/|DD|MM|YYYY/gi, m => chars[m])
          );
          return positivDate + " avant notre ère";
        } else {
          formatedDate = moment(date, baseFormat);
          return formatedDate.format(
            format.replace(/\/|DD|MM|YYYY/gi, m => chars[m])
          );
        }
      } else {
        return date;
      }
    },

    /**
     * Formate le nom et le prénom en le traduisant dans la langue courant
     * au format: "Nom, Prénom"
     * @param {string} name le nom
     * @param {string} firsName le prénom
     */
    getDigestNameTraduct(name, firsName) {
      let nameRes = "";
      if (name || firsName) {
        if (name) {
          nameRes = this.getTraduction(name);
        }
        if (name && firsName) {
          nameRes += " ";
        }
        if (firsName) {
          nameRes += this.getTraduction(firsName);
        }
      }
      return nameRes;
    },
    /**
     * Transforme le Json du digest en chaine de caractères
     * @param {Object} digest le digest à transformer
     * */
    setMetaDescription(digest) {
      // ==== Generation des paramètres partagés ==== //
      const digestDates = this.getDate(digest.startDate, digest.endDate);
      const dates = digestDates ? " - Dates: " + digestDates : "";
      const title = digest.title ? " - " + digest.title : "";
      const autor =
        digest.usualName || digest.usualFirstName
          ? " - Auteur: " + digest.usualName
          : "";
      const place = digest.place ? " - Lieu: " + digest.place : "";

      let description = this.getLabel(digest.noticeType);
      if (digest.noticeType === "ARTWORK") {
        // ==== Generation de la description pour une Oeuvre ==== //
        description += digest.artworkType ? " / " + digest.artworkType : "";
        description += title;
        description += autor;
        const datation = this.getDate(
          digest.datationExecutionStart,
          digest.datationExecutionEnd
        );
        description += datation ? " - Date de création: " + datation : "";
        description += place;
        description += digest.shelfMark
          ? " - Cote / Numéro: " + digest.shelfMark
          : "";
        description += digest.shelfMarkType ? digest.shelfMarkType : "";
      } else if (digest.noticeType === "COLLECTION") {
        // ==== Generation de la description pour une Collection ==== //
        description += digest.collectionType
          ? " / " + digest.collectionType
          : "";
        description += title;
        description +=
          digest.usualName || digest.usualFirstName
            ? " - Collectionneur: " + digest.usualName
            : "";
        description += dates;
      } else if (digest.noticeType === "PERSON") {
        // ==== Generation de la description pour une Personne ==== //
        description += digest.personType ? " / " + digest.personType : "";
        description += title;
        description += dates;
        description += digest.activityType
          ? " - Activité: " + digest.activityType
          : "";
        description += place;
      } else if (digest.noticeType === "REFERENCE") {
        // ==== Generation de la description pour une référence ==== //
        description += digest.referenceType ? " / " + digest.referenceType : "";
        description += title;
        description += autor;
        description += digest.journalTitle
          ? " - Titre de la revue: " + digest.journalTitle
          : "";
        description += digest.editionPlace
          ? " - Lieu d'édition: " + digest.editionPlace
          : "";
        description += digest.editor ? " - Editeur: " + digest.editor : "";
        description += dates;
        description += digest.institution
          ? " - Institution: " + digest.institution
          : "";
        description += digest.shelfMark ? " - Cote: " + digest.shelfMark : "";
      } else if (digest.noticeType === "EVENT") {
        // ==== Generation de la description pour une Evènement ==== //
        description += digest.eventType ? " / " + digest.eventType : "";
        description += title;
        description += dates;
        description += place;
      }
      return description;
    },

    /**
     * Renvoie le texte coupé, jusqu'à 'max' caractères,
     * auquel se rajoute le texte 'ellipsis'.
     * @param {*} text le texte à couper
     * @param {*} max le nombre max de caractères à garder dans text
     * @param {*} ellipsis le texte de fin de l'ellipse
     */
    crop(text, max = 50, ellipsis = "[...]") {
      if (text && typeof text === "string") {
        let result = text.trim();
        if (result.length > max) {
          result = `${result.substr(0, max)} ${ellipsis}`;
        }
        return result ?? "";
      }
      return "";
    },

    /**
     * Redimensionne le texte pour qu'il ne dépasse dans le digest
     * @param {string} text le texte à redimensionner
     * @param {number} chars nombre de caractères à ne pas dépasser
     */
    getTextResized(text, chars) {
      if (!text) {
        return "";
      }
      text = text.trim();
      if (text.length > chars) {
        return text.substr(0, chars) + " [...]";
      } else {
        return text;
      }
    },

    /**
     * Enlève les balises html présentes dans le texte puis le
     * réduis selon le nombre de caractères choisi
     * @param {string} text texte à nettoyer et redimensionner
     * @param {number} chars nombre de caractères à ne pas dépasser
     * @returns {string} text nettoyé et redimensionné
     */
    getTextCleanedAndResized(text, chars) {
      const textCleaned = this.getTextCleaned(text);
      return this.getTextResized(textCleaned, chars);
    },
    /**
     * Enlève les balises html présentes dans le texte
     * @param {string} text texte à nettoyer
     * @returns {string} text nettoyé
     */
    getTextCleaned(text) {
      return text.replace(/(<([^>]+)>)/gi, "");
    },
    upperFirst(str) {
      return str.charAt(0).toUpperCase() + str.slice(1);
    },
    titleIsEmpty(title) {
      if (title != null) {
        title = title.toLowerCase();
        title = title.replace(/ /g, "");
        title = title.replace(/[ÈÉÊËèéêë]/g, "e");
        title = title.replace(/[à]/g, "a");
      }
      return !title;
    },

    /**
     * Récupération des médias associés à une notice
     */
    async getMedia(id, mode) {
      const url = `/notice/${id}/media?mode=${mode}`;
      const result = await this.$axios.$get(url);
      return result;
    },

    /**
     * Donne le nombre de bases de données associées à la notice, ou
     * le nom de la base s'il n'y en a qu'une
     * @param {array} databases la liste des noms des bases de données
     */
    getDatabasesNumber(databases) {
      // database undefined pour notice news liées à aucune base de données
      if (!databases || databases.length === 0) {
        return "aucune base de données";
      }
      if (databases.length > 1) {
        return databases.length + " bases de données";
      }
      return " la base " + databases[0];
    },
    /**
     * Retourne la date au format "dd/MM/yyyy"
     * @param {string} date la date à formater
     */
    formatDate(date) {
      return date ? _formatDate(new Date(date), "dd/MM/yyyy") : null;
    },
    /**
     * Associe une couleur à un type de notice
     * @param noticeType
     * @returns {*}
     */
    getCouleur(noticeType) {
      return associationCouleurEnum[noticeType];
    },
    /**
     * Retire les tags HTML d'une string
     * @param string
     * @returns {string|string}
     */
    stripHTML(string) {
      const div = document.createElement('div');
      div.innerHTML = string;
      const text = div.textContent || div.textContent || '';
      return text;
    },
    /**
     * Déplacer un élément dans un tableau
     * @param arr
     * @param fromIndex position initiale
     * @param toIndex position souhaitée
     * @returns {*}
     */
    moveItemInArray(arr, fromIndex, toIndex) {
      const element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
      return arr;
    }
  }
};
