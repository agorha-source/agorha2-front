export default {
  data() {
    return {
      facetsArray: []
    };
  },
  methods: {
    parseEntry(entry, i = 1) {
      // Si la facette a une sous entrée
      if (entry.subEntries) {
        // On sauvegarde la facette
        this.saveFacet(entry, i);
        // Puis on itère sur la facette en sous entrée
        this.parseEntry(entry.subEntries, i + 1);
      } else if (entry[0]) {
        // Il peut y avoir plusieurs sous facettes donc on boucle sur les possibles subEntries présentes
        for (let j = 0; j < entry.length; j++) {
          this.parseEntry(entry[j], i);
        }
      } else {
        this.saveFacet(entry, i);
      }
    },
    saveFacet(facet, iteration) {
      /*
          Création d'un tableau qui récupère les différentes dates dans la value
          Ex: "/6e siècle avant notre ère/2e moitié du 6e siècle avant notre ère/4e quart du 6e siècle avant notre ère"
          retourne [0: "", 1: "6e siècle avant notre ère", 2: "2e moitié du 6e siècle avant notre ère", 3: "4e quart du 6e siècle avant notre ère"]
      */
      const splittedFacet = this.splitFacet(facet);
      if (splittedFacet.length === 1) {
        this.facetsArray.push({ Entry: facet.value, count: facet.count, level: 1 });
      } else {
        this.facetsArray.push({
          Entry: splittedFacet[iteration],
          count: facet.count,
          level: iteration
        });
      }
    },
    /**
     * @private
     */
    splitFacet(facet) {
      return facet.value.split("/");
    }
  }
};
