export const state = () => ({
  apiLoading: false
});

// Sync operations
export const mutations = {
  START_LOADING(state) {
    state.apiLoading = true;
  },
  FINISH_LOADING(state) {
    state.apiLoading = false;
  }
};

// Async operations
export const actions = {
  async startLoading({ commit }, payload) {
    // Pour voir les appels au back
    // console.info("API call:", payload);
    await commit("START_LOADING");
  },
  async finishLoading({ commit }) {
    await commit("FINISH_LOADING");
  }
};

export const getters = {
  getApiLoading: state => state.apiLoading
};
