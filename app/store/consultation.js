export const state = () => ({
  notice: {
    id: "",
    title: "",
    noticeType: "",
    previous: {
      id: "",
      title: "",
      thumbnail: ""
    },
    next: {
      id: "",
      title: "",
      thumbnail: ""
    },
    reference: [],
    linkedNotices: [],
    internal: {
      digest: {}
    },
    content: {},
    medias: [],
    prefPicture: {},
    thumbnail: ""
  },
  showDisplayAllButton: false,
  lastSearchRoute: {}
});

// Sync operations
export const mutations = {
  setNotice(state, payload) {
    const { notice, searchResults } = payload;
    const { internal, content } = notice;

    state.notice.content = content;
    if (internal) {
      state.notice.internal = internal;
      const noticeId = internal.uuid;
      state.notice.id = noticeId;
      state.notice.noticeType = internal.noticeType;
      state.notice.title = internal.digest?.title;
      // Identifiants des notices liées
      state.notice.reference = internal?.reference ?? [];

      // Récupération des résultats de recherche "précédent" et "suivant"
      searchResults.forEach((result, index) => {
        // Position dans les résultats de recherche
        if (result.id === noticeId) {
          const previous = searchResults[index - 1];
          const next = searchResults[index + 1];
          state.notice.previous.id = previous?.id;
          state.notice.previous.title = previous?.digest?.title;
          state.notice.previous.thumbnail =
            previous?.content?.mediaInformation?.prefPicture?.thumbnail;
          state.notice.next.id = next?.id;
          state.notice.next.title = next?.digest?.title;
          state.notice.next.thumbnail =
            next?.content?.mediaInformation?.prefPicture?.thumbnail;
        }
      });
    }
    // Récupération de l'image préférée si elle existe
    const prefPicture = notice.content?.mediaInformation?.prefPicture;
    if (prefPicture) {
      state.notice.prefPicture = prefPicture;
      if (prefPicture.thumbnail) {
        state.notice.thumbnail = prefPicture.thumbnail;
      }
    }
  },

  /** Mise à jour des médias de la notice */
  setMedias(state, medias) {
    state.notice.medias = [].concat(medias);
  },

  /** Mise à jour des fichiers de médias de la notice */
  setMediaFiles(state, mediaFiles) {
    state.notice.mediaFiles = [].concat(mediaFiles);
  },

  /** Mise à jour des notices liées */
  setLinkedNotices(state, notices) {
    state.notice.linkedNotices = [].concat(notices);
  },
  /** Mise à jour du statut d'affichage du bouton "Afficher tout" */
  setShowDisplayAllButton(state, visibleOrNot) {
    state.showDisplayAllButton = visibleOrNot;
  },
  /** Mise à jour de la dernière route de recherche empruntée */
  setLastSearchRoute(state, route) {
    state.lastSearchRoute = route;
  }
};

// Async operations
export const actions = {
  /**
   * Récupération de toutes les données de la notice
   * avec ses images et notices liées, par son identifiant.
   */
  async fetchNotice(context, id) {
    let notice = {};
    notice = await this.$axios.$get(`/notice/consultation/${id}`, { withCredentials: true })
      .catch((error) => {
        console.warn(`Erreur de récupération de la notice : ${id}`, error);
        // Lancer l'erreur jusqu'au CMS (par exemple les 404)
        return Promise.reject(error);
      });
    // Pour avoir accès aux notices précédente et suivante
    let searchResults = [];
    const selectedResults = context.rootGetters["recherche/getSelectedResults"];
    // Une sélection est en cours de consultation
    if (selectedResults.length > 0) {
      searchResults = selectedResults; // les résultats sont la sélection
    } else {
      // Dans le cas normal, on parcourt les résultats de recherche
      searchResults = context.rootState.recherche.searchResponse.results;
    }
    const payload = { notice, searchResults };
    context.commit("setNotice", payload);
    /** Récupération des images de la notice */
    const mediasUrl = `/notice/${id}/media?mode=FULL`;
    let medias = [];
    medias = await this.$axios
      .$get(mediasUrl, { progress: false })
      .catch((error) => {
        console.error(`Erreur de récupération des médias de la notice : ${id}`, error);
      });

    /**
     * Calcul du tableau mediaFiles de tous les fichiers de medias,
     * où chaque fichier est enrichi de l'info credit et licence.
     * On y conserve aussi l'uuid du media pour construire le canvasId.
     * (i) Le canvasId servant à la visionneuse est de la forme :
     *  - manifestBaseUrl/mediaUUID_fileUUID/canvas.json
     * (i) L'image préférée est placée en premier dans ce tableau
     */

    if (medias) {
      context.commit("setMedias", medias);
      const temp = medias
        .map(x =>
          x.files.map(file => Object({
            ...file,
            credit: x.credit,
            licence: x.licence,
            mediaUUID: x.internal.uuid
          }))
        )
        .flat();
      const prefPicture = context.state.notice.prefPicture;
      const prefPictureId = prefPicture?.idFile;
      const prefPictureIndex = temp.findIndex(f => f.uuid === prefPictureId);
      let mediaFiles = [];
      if (prefPictureIndex !== -1) {
        const prefPicture = temp[prefPictureIndex];
        temp.splice(prefPictureIndex, 1);
        mediaFiles = [prefPicture, ...temp];
      } else {
        mediaFiles = temp;
      }
      context.commit("setMediaFiles", mediaFiles);
    }
    /** Récupération des données (titre et miniature) des notices liées */
    var linkedNotices = [];
    linkedNotices = await this.$axios.$post(`/notice/getMosaicCards`, context.state.notice.reference , { headers: {'Accept': 'application/json'}, progress: false, withCredentials: true })
      .catch((error) => {
        console.error(`Erreur lors de la récupération des notices liées `);
      });
    context.commit("setLinkedNotices", linkedNotices);
  }
};

export const getters = {
  getNotice: state => state.notice,
  getShowDisplayAllButton: state => state.showDisplayAllButton,
  getLastSearchRoute: state => state.lastSearchRoute
};
