const defaultAutocompleteResults = () => {
  return {
    ARTWORK: [],
    PERSON: [],
    REFERENCE: [],
    EVENT: [],
    COLLECTION: [],
    NEWS: []
  };
};

export const state = () => ({
  // Les résultats d'autocomplétion rangés par type de notice
  autocompleteResults: defaultAutocompleteResults()
});

export const mutations = {
  /** Remise à l'état initial des résultats d'autocomplétion */
  resetAutocompleteResults(state) {
    Object.assign(state.autocompleteResults, defaultAutocompleteResults());
  },
  /** Ajoute un résultat d'autocomplétion au state, selon son type de notice */
  pushToAutocompleteResults(state, result) {
    const noticeType = result.digest.noticeType;
    if (noticeType) {
      state.autocompleteResults[noticeType].push(result);
    }
  }
};

export const actions = {
  /** Autocomplétion */
  async autocomplete(context, query) {
    const urlParams = {
      terms: query.terms,
      noticePageSize: 5,
      lang: "fre",
      noticeType: query.noticeType
    };
    const queryString = new URLSearchParams(urlParams);
    const autocompleteResponse = await this.$axios.$get(
      `/notice/autocomplete?${queryString}`,
      { progress: false } // pas de barre de chargement
    );
    context.commit("resetAutocompleteResults");
    for (const i in autocompleteResponse) {
      context.commit("pushToAutocompleteResults", autocompleteResponse[i]);
    }
  }
};

export const getters = {
  getAutocompleteResults: state => state.autocompleteResults
};
