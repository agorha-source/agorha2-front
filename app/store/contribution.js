const jp = require("jsonpath-faster");

export const state = () => ({
  // Notice avant toute modification
  intialNotice: {
    content: {}
  },

  notice: {
    content: {} // nécessaire pour editNoticeValue
  },
  noticeType: "",
  database: {}, // base de données d'édition de la notice
  extractedValues: [], // [{ path:String, value:Any }]
  isEditingNotice: false,
  paths: [], // le path de chacun des champs extraits,

  nestedContribution: false, // création à la volée en cours ?
  // Notice créée à la volée
  nestedNotice: {
    content: {} // nécessaire pour editNoticeValue
  },
  nestedNoticeType: "", // type de la notice créée à la volée
  nestedDatabase: {}, // base de données d'édition de la notice créée à la volée

  unicityFields: [] // les champs requis sur lesquels vérifier l'unicité de la notice
});

export const actions = {
  /** Récupérer la notice par son identifiant. */
  async fetchNotice(context, id) {
    const notice = await this.$axios
      .$get(`/notice/${id}`, { headers: {'Accept': 'application/json'}, withCredentials: true })
      .catch((error) => {
        console.error(`Impossible de récupérer la notice ${id}, ${error}`);
      });
    const noticeType = notice?.internal?.noticeType ?? "";
    context.commit("updateNotice", notice);
    context.commit("updateInitialNotice", Object.freeze(notice));
    context.commit("updateNoticeType", noticeType);
  },

  /** Récupération de l'objet base de données par son identifiant (numérique) */
  async fetchDatabase(context, key) {
    const database = await this.$axios
      .$get(`/database/${key}`)
      .catch(error => handleDatabaseError(error, key));
    if (database) {
      const uniqueKey = database.internal.uniqueKey;
      const contentTitle = database.content.title;
      if (uniqueKey && contentTitle) {
        context.commit("updateDatabase", {
          uuid: String(uniqueKey),
          title: String(contentTitle)
        });
      }
    }
  },

  /**
   * Demander au serveur le changement de statut
   * d'une ou plusieurs notices par leur identifant.
   */
  async changeNoticeStatus(context, options) {
    const { noticeIds, status } = options;
    const url = `/contribution/notices/status`;
    const request = {
      ids: noticeIds,
      status
    };
    const response = await this.$axios
      .$patch(url, request, { withCredentials: true })
      .catch((error) => {
        console.error(`Impossible de changer le statut des notices ${noticeIds.join(", ")}, ${error}`);
        return Promise.reject(error);
      });
    return response;
  },

  /**
   * Demander au serveur une duplication de notice.
   * - Erreur 404 si un identifiant (de base ou de notice) est introuvable.
   */
  async duplicateNotice(context, options) {
    const { noticeId, databaseId } = options;
    const url = `/contribution/notices/${noticeId}/duplicate?databaseId=${databaseId}`;
    const response = await this.$axios
      .$post(url, {}, { withCredentials: true })
      .catch((error) => {
        console.error(`Impossible de dupliquer la notice ${noticeId}, ${error}`);
        return Promise.reject(error);
      });
    return response;
  },

  /**
   * Demander au serveur une suppression de notice par son identifiant.
   * - Erreur 403 si la demande de suppression est refusée, lorsque
   * la notice des références ou des médias par exemple.
   * - Erreur 404 si la notice est introuvable.
   */
  async deleteNotice(context, id) {
    const url = `/contribution/notices/${id}`;
    const response = await this.$axios
      .$delete(url, { withCredentials: true })
      .catch((error) => {
        console.error(`Impossible de supprimer la notice ${id}, ${error}`);
        return Promise.reject(error);
      });
    return response;
  },

  /**
   * Récupérer une valeur dans la notice en modification.
   * Ajout à l'historique des valeurs trouvées.
   */
  findValueInNotice(context, path) {
    let notice = null;

    if (context.state.nestedContribution) {
      notice = context.state.nestedNotice;
    } else {
      notice = context.state.notice;
    }
    const found = findValue(path, notice);
    context.commit("addToExtractedValues", { path, value: found });
    return found;
  }
};

export const mutations = {
  /** Mettre à jour la notice */
  updateNotice(state, notice) {
    if (state.nestedContribution) {
      state.nestedNotice = notice;
    } else {
      state.notice = notice;
    }
  },
  /** Mettre à jour la notice avant toute modification */
  updateInitialNotice(state, notice) {
    if (notice) {
      state.intialNotice = JSON.parse(JSON.stringify(notice));
    }
  },
  /** Mettre à jour le type de la notice */
  updateNoticeType(state, noticeType) {
    if (state.nestedContribution) {
      state.nestedNoticeType = noticeType;
    } else {
      state.noticeType = noticeType;
    }
  },
  /** Définir le type de notice lors d'une création à la volée */
  setNestedNoticeType(state, noticeType) {
    state.nestedNoticeType = noticeType;
  },
  /** Mettre à jour la base de données */
  updateDatabase(state, database) {
    if (state.nestedContribution) {
      state.nestedDatabase = database;
    } else {
      state.database = database;
      // On suppose que la base de contribution à la volée
      // doit être la même que la base courante
      state.nestedDatabase = database;
    }
  },
  /** Mettre à jour les valeurs extraites de la notice modifiée */
  setExtractedValues(state, values) {
    state.extractedValues = values;
  },
  /** Garder une valeur extraite de la notice modifiée */
  addToExtractedValues(state, value) {
    if (!state.paths.includes(value.path)) {
      state.paths.push(value.path);
      state.extractedValues.push(value);
    }
  },
  /**
   * Indiquer à l'application qu'on
   * est en train de modifier une notice.
   */
  setEditingNoticeTrue(state) {
    state.isEditingNotice = true;
  },
  /**
   * Indiquer à l'application qu'on
   * N'est PAS en train de modifier une notice.
   */
  setEditingNoticeFalse(state) {
    state.isEditingNotice = false;
    state.extractedValues = [];
  },

  /**
   * Indiquer à l'application qu'une contribution
   * à la volée est en cours ou non.
   */
  setNestedContribution(state, booleanValue) {
    state.nestedContribution = booleanValue;
  },

  /** Mise à jour des champs requis sur lesquels vérifier l'unicité de la notice */
  setUnicityFields(state, unicityFields) {
    // FIXME: n'ajouter que les éléments ayant un attribut required:true
    // difficile à contrôler ici car le tableau est rempli de manière asynchrone
    state.unicityFields = unicityFields;
  }
};

export const getters = {
  /** La notice avant toute modification (état initial) */
  getInitialNotice: state => state.intialNotice,
  /** Les informations de notice à sauvegarder/modifier */
  getNotice: state => state.nestedContribution ? state.nestedNotice : state.notice,
  /** Le type de la notice à sauvegarder/modifier */
  getNoticeType: state => state.nestedContribution ? state.nestedNoticeType : state.noticeType,
  /** Le type de la notice à créer à la volée */
  getNestedNoticeType: state => state.nestedNoticeType,
  /** La base de données dans laquelle la notice est modifiée */
  getDatabase: state => state.nestedContribution ? state.nestedDatabase : state.database,
  /** Indique si une notice est en train d'être modifiée (et qu'elle n'est pas en contribution à la volée) */
  isEditingNotice: state => state.isEditingNotice && !state.nestedContribution,
  /** Compter le nombre de sous-objets au path dans la notice */
  countValuesAt: state => path => countValues(path, state.notice),
  /**
   * La liste des valeurs extraites de la notice
   * pour les renseigner dans le formulaire de modification.
   */
  getExtractedValues: state => state.extractedValues,
  /** Indique si une contribution à la volée est en cours */
  isNestedContribution: state => state.nestedContribution,
  /** Les champs requis sur lesquels vérifier l'unicité de la notice */
  getUnicityFields: state => state.unicityFields
};

/**
 * Récupère le sous-objet de la notice au path demandé.
 * Si non trouvé, renvoie undefined.
 */
function findValue(path, notice) {
  /**
   * Création d'un JSONPath à partir du path
   * (i) path a un format spécifique à l'application
   */
  if (!path) { return ""; }
  const jsonpath = path.replace(/\[\]/g, "");
  const value = jp.value(notice.content, jsonpath);
  return value;
}

/** Compter le nombre de sous-objets au path demandé. */
function countValues(path, notice) {
  if (!path) { return 0; }
  const stringPath = path.replace(/\[\]/g, "");
  const jsonpath = `${stringPath}.*`;
  const values = jp.query(notice.content, jsonpath);
  return values.length;
}

function handleDatabaseError(error, key) {
  const code = parseInt(error.response && error.response.status);
  const genericErrorMessage =
    `Impossible de récupérer la base de données ${key}`;
  const authErrorMessage =
    `Vous n'avez pas les droits pour accéder à la base de données ${key}`;
  if (code === 401) {
    console.error("Accès refusé", authErrorMessage);
  } else {
    console.error("Erreur réseau", genericErrorMessage);
  }
  return Promise.reject(error);
}
