import { DISPLAY_TYPE, SEARCH_TYPE } from "@/const/_recherche";
import {
  encode,
  decode,
  computeAggregations,
  aggregationsToCheckedNodes
} from "@/helpers/facetHelper";
import { frontFilterToBackFilter } from "~/helpers/filterHelper";

// Query parameters initiaux
const defaultSearchParams = () => {
  return {
    terms: "",
    page: 1,
    pageSize: 20,
    sort: "",
    fieldSort: "",
    noticeType: "",
    type: SEARCH_TYPE.simple,
    selectionId: "",
    affichage: DISPLAY_TYPE.cartouche,
    facets: "", // (base64)
    /** Front filters : n'est rempli qu'en cas de recherche avancée */
    filters: "", // (base64)
    withAttachment: false
  };
};

const defaultContentTarget = "NOTICE";

// L'état initial des onglets
const defaultTabs = () => {
  return {
    counting: [
      { value: "ALL", count: 0 },
      { value: "ARTWORK", count: 0 },
      { value: "PERSON", count: 0 },
      { value: "EVENT", count: 0 },
      { value: "REFERENCE", count: 0 },
      { value: "COLLECTION", count: 0 },
      { value: "NEWS", count: 0 }
    ],
    // L'onglet sélectionné
    selectedTab: "ALL"
  };
};

/** Valeur par défaut des aggregations, un filtre d'onglet en option */
const defaultAggregations = (tab = undefined) => {
  let filter = "";
  if (tab !== "ALL" && tab) { filter = tab; }
  return {
    // Facette de l'onglet
    noticeType: {
      filters: [filter], // on peut définir un onglet par défaut
      field: "internal.noticeType",
      facetScope: "tab",
      facetOrder: 0,
      pathHierarchical: false
    }
  };
};

export const state = () => ({
  // Query parameters
  searchParams: defaultSearchParams(),
  // L'état des onglets
  tabs: defaultTabs(),
  // Objet de résultat de recherche
  searchResponse: {
    // Liste des résultats de la recherche en cours
    results: [],
    facets: [],
    // Métadonnées de la recherche
    offset: 0,
    limit: 0,
    count: 0,
    selectionBelongToUser: false
  },
  // Les searchParams.terms de la recherche qui vient d'être effectuée
  currentTerms: "",
  // Pour l'export CSV, on appelle les méthodes d'export
  // avec les mêmes paramètres que la recherche en cours.
  lastSearchParams: "",

  // Cases cochées dans les arbres de facettes générales et spécifiques
  checkedNodes: [],
  // Aggregations (pour les facettes)
  aggregations: defaultAggregations(),

  /// Recherche avancée
  // - Filtres de recherche avancée
  filters: [],
  // - Cible de la recherche avancée
  contentTarget: defaultContentTarget,
  // Historique des recherches
  lastSearchTerms: [],
  // Ids des résultats sélectionnés lors d'une recherche
  selectedIds: [],
  // Résultats sélectionnés
  selectedResults: [],
  // Identifiant de base de donnée de contexte (lorsqu'une base est sélectionnée en facette)
  contextDatabaseId: ""
});

// Sync operations
export const mutations = {
  /** Remise à l'état initial des paramètres de recherche */
  resetSearchParams(state) {
    state.searchParams = defaultSearchParams();
    state.currentTerms = "";
    state.checkedNodes = [];
    state.aggregations = defaultAggregations();
    state.filters = [];
    state.contentTarget = defaultContentTarget;
    state.selectedIds = [];
  },
  /** Remise à l'état initial du compte des onglets */
  resetTabs(state) {
    state.tabs = defaultTabs();
  },
  /** Remise à l'état initial des aggregations, avec un onglet */
  resetAggregationsWithTab(state, tab) {
    state.aggregations = defaultAggregations(tab);
  },

  /** Mise à jour des paramètres de recherche */
  updateSearchParams(state, searchParams) {
    // Supprimer les valeurs null et undefined de l'objet

    // TODO cette modification apporte une régression sur la suppression d'un terme de recherche ou des facettes - A corriger
    // Object.keys(searchParams).forEach(k =>
    //   (!searchParams[k] && searchParams[k] !== undefined) &&
    //   delete searchParams[k]);

    // Mettre à jour les valeurs de searchParams dans le state
    Object.assign(state.searchParams, searchParams);
  },

  /** Mise à jour des termes de recherche */
  updateCurrentTerms(state, terms) {
    state.currentTerms = terms;
  },

  /** Mise à jour de l'objet d'aggregations (pour les facettes) */
  updateAggregations(state, aggregations) {
    state.aggregations = aggregations;
  },

  /** Mise à jour de l'onglet courant dans les aggregations */
  updateTabInAggregations(state, tab) {
    state.aggregations.noticeType.filters = [tab];
  },

  /** Mise à jour des filtres pour la recherche avancée */
  updateFilters(state, filters) {
    state.filters = filters;
  },

  /** Mise à jour des filtres pour la recherche avancée */
  updateContentTarget(state, contentTarget) {
    state.contentTarget = contentTarget;
  },

  /**
   * Mise à jour de l'onglet sélectionné.
   * Changer d'onglet devrait vider les facettes.
   */
  updateSelectedTab(state, selectedTab) {
    // Au clic sur le même onglet, ne rien faire
    if (selectedTab === state.tabs.selectedTab) { return; }
    state.tabs.selectedTab = selectedTab || "ALL";
  },

  /** Mise à jour des onglets */
  updateTabs(state, searchResponse) {
    // Remise à zéro du comptage des onglets
    state.tabs.counting = defaultTabs().counting;
    // Récupération des onglets dans les facettes
    const facets = searchResponse.facets ?? [];
    const tabEntries = facets.find(x => x.id === "noticeType")?.entries ?? [];
    let totalCount = 0;
    let hasResults = false;
    // Mise à jour du comptage par onglet
    tabEntries.forEach((entry) => {
      state.tabs.counting.find(o => o.value === entry.value).count =
        entry.count;
      totalCount += entry.count;
      if (entry.value === state.tabs.selectedTab) {
        hasResults = true;
      }
    });
    // Onglet "Tous les résultats"
    state.tabs.counting.find(o => o.value === "ALL").count = totalCount;
    // Si l'onglet courant n'a pas de résultat, on passe sur l'onglet "Tous les résultats"
    if (!hasResults) { state.tabs.selectedTab = "ALL"; }
  },

  /**
   * Ajout d'un terme à l'historique des recherches.
   * On ne garde que les 10 dernières recherches.
   */
  addLastSearchTerms(state, value) {
    if (value) {
      let items = state.lastSearchTerms;
      items = items.filter(item => item !== value);
      items.unshift(value);
      state.lastSearchTerms = items.slice(0, 10); // 10 maximum
    }
  },

  /** Mise à jour de l'historique de recherches */
  updateLastSearchTerms(state, lastSearchTerms) {
    state.lastSearchTerms = lastSearchTerms;
  },

  /** Mise à jour du résultat de la recherche */
  updateSearchResponse(state, searchResponse) {
    state.searchResponse = searchResponse;
    const { results, facets } = searchResponse;
    state.searchResponse.facets = [...facets];
    // S'assurer de vider la liste si aucun résultat
    if (!results || results.length === 0) {
      state.searchResponse.results = [];
      // Ne pas vider les facettes
    }
  },

  /**
   * Mise à jour des paramètres de la dernière
   * recherche pour l'export CSV.
   */
  updateLastSearchParams(state, params) {
    state.lastSearchParams = params;
  },

  /** La liste des résultats est restreinte à sélection passée en paramètres */
  setSelectedResults(state, results) {
    state.selectedResults = results;
  },

  /** Ajout d'un filtre de sélection d'un onglet */
  addTabFacet(state, value) {
    state.searchResponse.facets.push({
      data: {
        facetKey: "noticeType",
        key: value
      }
    });
  },

  /**
   * TODO: à modifer et utiliser pour remplacer
   * removeTabFacet + addTabFacet
   * Mise à jour de la facette de l'onglet courant
   */
  updateTabFacet(state, value) {
    const facets = state.searchResponse.facets;
    const index = facets.findIndex(
      facet => facet?.data?.facetKey === "noticeType"
    );
    Object.assign(facets[index]?.data?.key, value);
  },

  /** Supprime le filtre de sélection d'un onglet */
  removeTabFacet(state) {
    const facets = state.searchResponse.facets;
    const index = facets.findIndex(
      facet => facet?.data?.facetKey === "noticeType"
    );
    if (index !== -1) { facets.splice(index, 1); }
  },

  /** Ajoute un id de notice à la liste des résultats sélectionnés */
  addToSelectedIds(state, id) {
    if (!state.selectedIds.includes(id)) {
      state.selectedIds.push(id);
    }
  },

  /** Supprime un id de notice de la liste des résultats sélectionnés */
  removeFromSelectedIds(state, id) {
    const sr = state.selectedIds;
    const index = sr.findIndex(i => i === id);
    if (index !== -1) { sr.splice(index, 1); }
  },

  /** Définit checked à true pour tous les résultats de recherche */
  checkAllResults(state) {
    const ids = state.searchResponse.results.map(x => x.id);
    state.selectedIds = ids;
  },

  /** Rajoute la propriété visited:true à un résultat de recherche */
  setNoticeVisited(state, id) {
    const { results } = state.searchResponse;
    const targetResult = results.find(x => x.id === id);
    if (targetResult) {
      targetResult.visited = true;
    }
  },

  /**
   * Rajoute les filtres aux aggregations d'après les noeuds
   * cochés dans les arbres de facettes.
   */
  updateAggregationsWithCheckedNodes(state, checkedNodes) {
    for (const node of checkedNodes) {
      const { facetKey, key } = node;
      const filters = state.aggregations?.[facetKey]?.filters;
      if (filters && !filters.includes(key)) { filters.push(key); }
    }
  },

  /** Mise à jour des noeuds déjà cochés */
  setCheckedNodes(state, nodes) {
    state.checkedNodes = nodes;
  },

  /**
   * Supprime un filtre des aggregations à partir des filtres courants.
   * @param {Object} state un état du store
   * @param {Object} nodeFilter un filtre au format { id, facetKey, key }
   */
  removeCheckedNode(state, nodeFilter) {
    // Suppression dans checkedNodes s'il existe
    const { id, facetKey, key } = nodeFilter;
    const filtered = state.checkedNodes.filter(n => n.id !== id);
    state.checkedNodes = filtered;
    // Suppression dans aggregations
    const filters = state.aggregations[facetKey]?.filters;
    if (filters) {
      const index = filters.indexOf(key);
      if (filters && index !== -1) { filters.splice(index, 1); }
    }
  },

  /**
   * Supprime un filtre des aggregations à de l'arbre des facettes.
   * @param {Object} state un état du store
   * @param {Node} node un noeud LiquorTree (https://amsik.github.io/liquor-tree/#Structure)
   */
  removeCheckedNodeFromTree(state, node) {
    const { facetKey, key } = node.data;
    // Suppression dans checkedNodes s'il existe
    const filtered = state.checkedNodes.filter(
      n => n.facetKey !== facetKey && n.key !== key
    );
    state.checkedNodes = filtered;
    const filters = state.aggregations[facetKey]?.filters;
    if (filters) {
      const index = filters.indexOf(key);
      if (filters && index !== -1) { filters.splice(index, 1); }
    }
  },

  /** Met à jour l'identifiant de la base de données de contexte */
  updateContextDatabaseId(state, id) {
    state.contextDatabaseId = id;
  }
};

// Async operations
export const actions = {
  /**
   * @private
   * Préparation de la requête de recherche :
   * - Si le paramètre filters est rempli, prépare une recherche avancée
   * - Sinon, prépare une recherche simple
   * @returns {URLSearchParams} Les paramètres de recherche
   */
  prepareSearchParams(context) {
    // On garde les termes de la recherche en cours
    context.commit("updateCurrentTerms", context.state.searchParams.terms);
    const facets = context.state.searchParams.facets;
    let selectedTab = context.state.tabs.selectedTab;
    let significantAggs = {};

    // Récupération de l'onglet sélectionné depuis l'URL
    if (facets) {
      significantAggs = decode(facets);
      const currentTab = significantAggs.noticeType?.filters?.[0] ?? "";
      selectedTab = currentTab;
      context.commit("updateSelectedTab", selectedTab);
    }

    // Si un filtre sur le type de notice est actif, l'onglet est fixé sur ce type
    const noticeType = context.state.searchParams.noticeType;
    if (noticeType) {
      context.commit("updateSelectedTab", noticeType);
      selectedTab = noticeType;
    }

    // Le métamodèle récupéré pour les facettes doit être celui de l'onglet courant (selectedTab)
    // Pourquoi pas le filtre de notice (noticeType) ?
    // Car au clic sur un onglet, on souhaite avoir les facettes spécifiques associées
    // Ce qui ne serait pas le cas en utilisant noticeType !

    let schema = {};
    // NEWS n'a pas de métamodèle, on prend ARTWORK à la place
    if (selectedTab === "NEWS") {
      schema = context.rootGetters.getSchemaByKey("ARTWORK");
    } else {
      schema = context.rootGetters.getSchemaByKey(selectedTab);
    }

    // Récupération des facettes générales (et spécifiques, si onglet sélectionné)
    // dans le métamodèle du type courant
    const specific = selectedTab && selectedTab !== "ALL" && selectedTab !== "";
    const emptyAggregations = computeAggregations(schema, specific, selectedTab);
    // Ajout du filtre sur l'onglet
    if (specific) { emptyAggregations.noticeType.filters.push(selectedTab); }
    context.commit("updateAggregations", emptyAggregations);

    // Extraction des facettes depuis l'URL
    if (facets) {
      const nodesFromURL = aggregationsToCheckedNodes(significantAggs);
      context.commit("setCheckedNodes", nodesFromURL);
      // Ajout des noeuds cochés de l'arbre des facettes aux aggregations
      context.commit("updateAggregationsWithCheckedNodes", nodesFromURL);
    }

    // Extraction des filtres (recherche avancée) depuis l'URL
    const encodedFilters = context.state.searchParams.filters;
    if (encodedFilters) {
      const activeFilters = decode(encodedFilters);
      context.commit("updateFilters", activeFilters);
    }
    // Vider une éventuelle sélection en cours
    context.commit("setSelectedResults", []);
  },

  /**
   * @public
   * Unique point d'entrée de la recherche :
   * - Si filters est rempli, appelle une recherche avancée
   * - Sinon, appelle une recherche simple
   */
  async search(context, mapFields) {
    let searchResponse = {};
    await context.dispatch("prepareSearchParams");
    // Récupération de l'identifiant de base de données de contexte
    await context.dispatch("findContextDatabaseId", context.state.aggregations);
    const { searchParams, filters } = context.state;
    // Recherche avancée (quand il y a des filters)
    if (filters.length > 0) {
      const params = {};
      params.pageSize = searchParams.pageSize ?? defaultSearchParams.pageSize;
      params.noticeType = searchParams.noticeType ?? defaultSearchParams.noticeType;
      params.selectionId = searchParams.selectionId ?? defaultSearchParams.selectionId;
      params.sort = searchParams.sort ?? defaultSearchParams.sort;
      params.page = searchParams.page ?? defaultSearchParams.page;
      params.aggregations = context.state.aggregations ?? defaultAggregations();
      params.contentTarget = context.state.contentTarget ?? defaultContentTarget;
      params.filters = frontFilterToBackFilter(filters) ?? defaultSearchParams.filters;

      if (context.state.searchParams.affichage === "map" && mapFields) {
        params.fields = mapFields;
      }

      const filtersForURL = encodeURIComponent(encode(params));
      const url = `/notice/advSearch?filters=${filtersForURL}`;
      searchResponse = await this.$axios.$get(url);
      // Sauvegarde des paramètres d'URL si besoin pour l'export CSV
      context.commit("updateLastSearchParams", `filters=${filtersForURL}`);
    } else {
      // Recherche simple
      const urlSearchParams = new URLSearchParams(searchParams);
      // On remplace les facettes dans l'URL
      const aggs = { aggregations: context.state.aggregations };
      urlSearchParams.set("facets", encode(aggs));
      // Demander les informations de positions pour l'affichage map
      if (context.state.searchParams.affichage === "map" && mapFields) {
        for (const field of mapFields) {
          urlSearchParams.append("fields", field);
        }
      }

      const url = `/notice/search?${urlSearchParams.toString()}`;
      searchResponse = await this.$axios.$get(url);
      // Sauvegarde des paramètres d'URL si besoin pour l'export CSV
      context.commit("updateLastSearchParams", `${urlSearchParams.toString()}`);
    }

    // Post-traitement avant affichage
    if (searchResponse.results) {
      // Ajout de propriétés associées à l'UI pour les rendre réactives
      searchResponse.results.forEach((r) => {
        r.visited = false;
      });
    }
    // Mise à jour de la réponse de recherche
    context.commit("updateSearchResponse", searchResponse);
    // Mise à jour des onglets
    context.commit("updateTabs", searchResponse);
  },

  /**
   * Récupération de l'identifiant de la base de données du contexte
   * si l'objet aggregations de la requête de recherche contient une unique base.
   * @param {Object} state un état du store
   * @param {Object} aggs un objet aggregations de la requête de recherche
   */
  async findContextDatabaseId(context, aggs) {
    const dbAggregation = aggs["Bases de données"];
    if (dbAggregation) {
      const databases = dbAggregation.filters;
      if (databases && databases.length === 1) {
        if (databases[0] === "Non renseignée") {
          context.commit("updateContextDatabaseId", "");
          return;
        }
        const url = "/database/find";
        const db = await this.$axios.$post(url, { title: databases[0] })
          .catch((e) => { console.warn(`Base "${databases[0]}" introuvable`); return ""; }); // TESTER
        if (db) {
          const id = db.internal.uuid ?? "";
          context.commit("updateContextDatabaseId", id);
        }
      } else {
        context.commit("updateContextDatabaseId", "");
      }
    }
  },

  /** Récupération des données pour l'affichage frise */
  async timeline(context) {
    if (context.state.searchParams.type === "advanced") {
      const params = {
        pageSize: context.state.searchParams.pageSize ?? defaultSearchParams.pageSize,
        noticeType: context.state.searchParams.noticeType ?? defaultSearchParams.noticeType,
        selectionId: context.state.searchParams.selectionId ?? defaultSearchParams.selectionId,
        sort: context.state.searchParams.sort ?? defaultSearchParams.sort,
        page: context.state.searchParams.page ?? defaultSearchParams.page,
        aggregations: context.state.aggregations ?? defaultAggregations(),
        contentTarget: context.state.contentTarget ?? defaultContentTarget,
        filters: frontFilterToBackFilter(context.state.filters) ?? defaultSearchParams.filters
      };
      const filters = encodeURIComponent(encode(params));

      return await this.$axios
        .$get(
          `/notice/advTimeline?filters=${filters.toString()}`
        );
    } else {
      // Recherche simple
      const urlSearchParams = new URLSearchParams();

      // On remplace les facettes dans l'URL
      const aggs = { aggregations: context.state.aggregations };
      const selectionId = context.state.searchParams.selectionId;
      const pageSizeNumber = context.state.searchParams.pageSize;
      const terms = context.state.currentTerms;
      urlSearchParams.set("facets", encode(aggs));

      return await this.$axios
        .$get(
          `/notice/timeline?terms=${terms}&pageSize=${pageSizeNumber}&${urlSearchParams.toString()}&selectionId=${selectionId}`
        );
    }
  },
  // TODO: documentation
  async netChart(context, id) {
    return await this.$axios
      // TODO: à adapter en fonction des types de notice
      .$get(`/notice/graph?selectionId=${id[0]}&noticeId=${id[1]}`);
  },
  /** Récupérer l'association JsonPath / Type de lieu */
  async geoPointField(context) {
    return await this.$axios.get("notice/geoPointField");
  }
};

export const getters = {
  // Paramètres de recherche
  getSearchParams: state => state.searchParams,
  getNoticeType: state => state.searchParams.noticeType,
  getTerms: state => state.searchParams.terms,
  getSearchType: state => state.searchParams.type,
  // Résultats de recherche
  getSearchResponse: state => state.searchResponse,
  getSearchResponseFacets: state => state.searchResponse.facets,
  // La sélection appartient-elle à l'utilisateur
  getSelectionBelongToUser: state => state.searchResponse.selectionBelongToUser,
  // Termes de la recherche qui vient d'être effectuée
  getCurrentTerms: state => state.currentTerms,
  // Paramètres (query params) de la recherche qui vient d'être effectuée
  getLastSearchParams: state => state.lastSearchParams,
  // Facettes
  // -- Noeuds cochés
  getCheckedNodes: state => state.checkedNodes,
  // -- Noeuds cochés catégorisées
  getCheckedNodesByCategory: state => state.checkedNodes.reduce((x, y) => {
    (x[y.facetKey] = x[y.facetKey] || []).push(y);
    return x;
  }, {}),
  // -- Aggregations
  getAggregations: state => state.aggregations,
  // Filtres de recherche avancée
  getFilters: state => state.filters,
  getContentTarget: state => state.contentTarget,
  // Ids des résultats sélectionnés (cochés)
  getSelectedIds: state => state.selectedIds,
  // Résultats sélectionnés (cochés)
  getSelectedResults: state => state.selectedResults,
  // Onglets
  getSelectedTab: state => state.tabs.selectedTab,
  getTabsCounting: state => state.tabs.counting,
  // Historique des termes de recherche
  getLastSearchTerms: state => state.lastSearchTerms,
  // Identifiant de la base de données de contexte
  getContextDatabaseId: state => state.contextDatabaseId
};
