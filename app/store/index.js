// FIXME: résoudre les erreurs avec editNoticeValue
export const strict = false;

export const state = () => ({
  schemas: {
    ARTWORK: {},
    PERSON: {},
    EVENT: {},
    REFERENCE: {},
    COLLECTION: {}
    // NEWS n'a pas de métamodèle
  }
});

// Sync operations
export const mutations = {
  updateSchemas(state, schema) {
    if (schema) { Object.assign(state.schemas, schema); }
  }
};

// Async operations
export const actions = {
  async nuxtServerInit({ dispatch }) {
    await dispatch("loadSchemas");
  },
  async loadSchemas({ commit, state }) {
    /** Promesse de schéma en fonction du type de notice */
    const getSchema = noticeType =>
      this.$axios.$get(`/metamodeles?noticeType=${noticeType}`);

    const schemaPromises = [];
    const schemaKeys = Object.keys(state.schemas);
    schemaKeys.map((key) => {
      // Si le schéma est vide dans le state, on le charge
      if (Object.keys(state.schemas[key]).length === 0) {
        schemaPromises.push(getSchema(key));
      }
    });
    const schemas = await Promise.all(schemaPromises);
    const [a, p, e, r, c] = schemas;
    commit("updateSchemas", { ARTWORK: a });
    commit("updateSchemas", { PERSON: p });
    commit("updateSchemas", { EVENT: e });
    commit("updateSchemas", { REFERENCE: r });
    commit("updateSchemas", { COLLECTION: c });
  }
};

export const getters = {
  getSchemaByKey: state => (key) => {
    let actualKey = key;
    // Si la clé recherchée n'est pas dans la liste des modèles
    if (!Object.keys(state.schemas).includes(key)) {
      // On choisit ARTWORK comme schéma par défaut
      actualKey = "ARTWORK";
      console.warn(`Métamodèle '${key}' introuvable, métamodèle '${actualKey}' renvoyé par défaut`);
    }
    return state.schemas[actualKey];
  }
};
