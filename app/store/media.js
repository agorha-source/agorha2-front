import { aggregationsToCheckedNodes, decode, encode } from "~/helpers/facetHelper";
import { getMediaFacetsEncoded } from "~/helpers/mediaHelper";

const defaultSearchParams = () => {
  return {
    terms: "",
    page: 1,
    pageSize: 20,
    sort: "",
    fieldSort: "PERTINENCE",
    facets: "" // (base64)
  };
};

export const state = () => ({
  // Query parameters
  searchParams: defaultSearchParams(),
  // Objet de résultat de recherche
  searchResponse: {
    // Liste des résultats de la recherche en cours
    results: [],
    facets: [],
    // Métadonnées de la recherche
    offset: 0,
    limit: 0,
    count: 0,
    selectionBelongToUser: false
  },
  // Les searchParams.terms de la recherche qui vient d'être effectuée
  currentTerms: "",
  // Pour l'export CSV, on appelle les méthodes d'export
  // avec les mêmes paramètres que la recherche en cours.
  lastSearchParams: "",
  // Cases cochées dans les arbres de facettes générales et spécifiques
  checkedNodes: [],
  // Aggregations (pour les facettes)
  aggregations: {},
  // Médias cochés dans la page de recherche de médias
  checkedMedias: [],
  // Médias choisis lors de la création d'une notice
  selectedMedias: []
});

// Sync operations
export const mutations = {
  /** Remise à l'état initial des paramètres de recherche */
  resetSearchParams(state) {
    state.searchParams = defaultSearchParams();
    state.currentTerms = "";
    state.checkedNodes = [];
    state.aggregations = {};
  },
  /** Mise à jour des paramètres de recherche */
  updateSearchParams(state, searchParams) {
    Object.assign(state.searchParams, searchParams);
  },
  /** Mise à jour des termes de recherche */
  updateCurrentTerms(state, terms) {
    state.currentTerms = terms;
  },
  /** Mise à jour des noeuds déjà cochés */
  setCheckedNodes(state, nodes) {
    state.checkedNodes = nodes;
  },
  /** Mise à jour de l'objet d'aggregations (pour les facettes) */
  updateAggregations(state, aggregations) {
    state.aggregations = aggregations;
  },
  updateSelectedMedias(state, selectedMedias) {
    if (selectedMedias) {
      state.selectedMedias = selectedMedias;
    }
  },
  /**
   * Rajoute les filtres aux aggregations d'après les noeuds
   * cochés dans les arbres de facettes.
   */
  updateAggregationsWithCheckedNodes(state, checkedNodes) {
    for (const node of checkedNodes) {
      const { facetKey, key } = node;
      const filters = state.aggregations?.[facetKey]?.filters;
      if (filters && !filters.includes(key)) { filters.push(key); }
    }
  },
  /** Mise à jour du résultat de la recherche */
  updateSearchResponse(state, searchResponse) {
    state.searchResponse = searchResponse;
    const { results, facets } = searchResponse;
    state.searchResponse.facets = [...facets];
    // S'assurer de vider la liste si aucun résultat
    if (!results || results.length === 0) {
      state.searchResponse.results = [];
      // Ne pas vider les facettes
    }
  },
  updateCheckedMedias(state, checkedMedias) {
    state.checkedMedias = checkedMedias;
  },
  /**
   * Supprime un filtre des aggregations à partir des filtres courants.
   * @param {Object} state un état du store
   * @param {Object} nodeFilter un filtre au format { id, facetKey, key }
   */
  removeCheckedNode(state, nodeFilter) {
    // Suppression dans checkedNodes s'il existe
    const { id, facetKey, key } = nodeFilter;
    state.checkedNodes = state.checkedNodes.filter(n => n.id !== id);
    // Suppression dans aggregations
    const filters = state.aggregations[facetKey]?.filters;
    if (filters) {
      const index = filters.indexOf(key);
      if (filters && index !== -1) { filters.splice(index, 1); }
    }
  },
  /**
   * Supprime un filtre des aggregations à de l'arbre des facettes.
   * @param {Object} state un état du store
   * @param {Node} node un noeud LiquorTree (https://amsik.github.io/liquor-tree/#Structure)
   */
  removeCheckedNodeFromTree(state, node) {
    const { facetKey, key } = node.data;
    // Suppression dans checkedNodes s'il existe
    state.checkedNodes = state.checkedNodes.filter(
      n => n.facetKey !== facetKey && n.key !== key
    );
    const filters = state.aggregations[facetKey]?.filters;
    if (filters) {
      const index = filters.indexOf(key);
      if (filters && index !== -1) { filters.splice(index, 1); }
    }
  },
  /**
   * Remet à zéro les médias selectionnés
   */
  removeCheckedMedias(state) {
    state.checkedMedias = [];
  }
};

// Async operations
export const actions = {
  /**
   * @private
   * Préparation de la requête de recherche de média
   * @returns {URLSearchParams} Les paramètres de recherche
   */
  prepareMediaSearchParams(context) {
    // On garde les termes de la recherche en cours
    context.commit("updateCurrentTerms", context.state.searchParams.terms);
    // Récupère le schéma type des facettes dans la recherche de média
    context.commit("updateAggregations", getMediaFacetsEncoded());
    const facets = context.state.searchParams.facets;
    // Extraction des facettes depuis l'URL
    if (facets) {
      const significantAggs = decode(facets);
      const nodesFromURL = aggregationsToCheckedNodes(significantAggs);
      // Sauvegarde des noeuds cochés
      context.commit("setCheckedNodes", nodesFromURL);
      // Ajout des noeuds cochés de l'arbre des facettes aux aggregations
      context.commit("updateAggregationsWithCheckedNodes", nodesFromURL);
    }
  },
  /**
   * @public
   * Unique point d'entrée de la recherche de média
   */
  async search(context) {
    let searchResponse = {};
    // Vide les médias cochés
    context.commit("removeCheckedMedias");
    // Chaque recherche incrémente un index global pour la mise à jour des facettes
    await context.dispatch("prepareMediaSearchParams");
    const { searchParams } = context.state;
    // Recherche simple
    const urlSearchParams = new URLSearchParams(searchParams);
    // On remplace les facettes dans l'URL
    const aggs = { aggregations: context.state.aggregations };
    urlSearchParams.set("facets", encode(aggs));
    const url = `/contribution/media/search?${urlSearchParams.toString()}`;
    searchResponse = await this.$axios.$get(url, { withCredentials: true });
    // Mise à jour de la réponse de recherche
    context.commit("updateSearchResponse", searchResponse);
  }
};

export const getters = {
  // Paramètres de recherche
  getSearchParams: state => state.searchParams,
  getTerms: state => state.searchParams.terms,
  // Résultats de recherche
  getSearchResponse: state => state.searchResponse,
  getSearchResponseFacets: state => state.searchResponse.facets,
  // Facettes
  // -- Noeuds cochés
  getCheckedNodes: state => state.checkedNodes,
  // -- Noeuds cochés catégorisées
  getCheckedNodesByCategory: state => state.checkedNodes.reduce((x, y) => {
    (x[y.facetKey] = x[y.facetKey] || []).push(y);
    return x;
  }, {}),
  // -- Aggregations
  getAggregations: state => state.aggregations,
  // Médias
  // -- Médias checked
  getCheckedMedias: state => state.checkedMedias,
  // Contribution
  // -- Média sélectionné
  getSelectedMedias: state => state.selectedMedias
};
