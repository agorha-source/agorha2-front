export default function({ $auth, redirect, query }) {
  if ($auth.loggedIn) {
    const role = $auth.user.authorizations.role;
    if (role !== "ADMIN" && role !== "CONTRIB" && role !== "DATABASE_MANAGER") {
      redirect("/");
    }
  } else {
    redirect("/");
  }
}
