/**
 * TODO: remplacer par méthode unique
 * @deprecated
 * Récupère les bases de données existantes
 * @param {Object} axios
 */
export async function getDatabases(axios) {
  const results = await axios
    .get("/database/list")
    .then(response => response.data)
    .catch(e => {
      console.error(e);
    });
  return results;
}

/**
 * L'objet des onglets (pour BlocMulti) à partir du nombre d'onglets.
 * Exemples :
 * - si 0 ou 1 onglet : [0]
 * - si 3 onglets : [0,1,2]
 */
export function tabsWhenEditing(count) {
  const tabs =
    count === 0 ? [] : [...Object.keys(Array(count).fill(0))].map(Number);
  return tabs;
}

/**
 * Génère dans la notice de création les bloc d'information qui peuvent être remplis
 * @param {Array} pathArray tableau du chemin : contient le nom des champs à générer dans leur odre de création
 * @param {Number} index l'index permettant de parcourir le tableau du chemin
 * @param {Object} noticePath placement dans la notice à l'endroit ou le bloc doit être créé
 */
export function createPathInNotice(pathArray, index, noticePath) {
  if (pathArray[index].includes("[]")) {
    // c'est une liste
    const pathName = pathArray[index].replace("[]", "");
    // Teste l'existence du tableau avant d'éventuellement l'écraser
    noticePath[pathName] = noticePath[pathName] ? noticePath[pathName] : [];
  } else {
    // c'est un objet/une valeur
    if (!noticePath[pathArray[index]]) {
      if (pathArray[index + 1]) {
        noticePath[pathArray[index]] = {};
      } else {
        noticePath[pathArray[index]] = "";
      }
    }
    if (pathArray[index + 1]) {
      createPathInNotice(pathArray, index + 1, noticePath[pathArray[index]]);
    }
  }
}

/**
 * Insère la valeur passée en paramètre dans un champ de la notice.
 * @param {Array} pathArray tableau contenant le chemin du champ à remplir
 * @param {Number} index l'index permettant de parcourir le tableau du chemin
 * @param {Object} notice la notice à remplir
 * @param {String} fieldValue la valeur à insérer
 */
export function editNoticeValue(pathArray, index, notice, fieldValue) {
  const pathName = pathArray[index].replace("[]", "");
  if (pathArray[index].includes("[]")) {
    if (!notice[pathName]) {
      notice[pathName] = [];
    }
    if (!notice[pathName][pathArray[index + 1]]) {
      notice[pathName][pathArray[index + 1]] = {};
    }
    editNoticeValue(
      pathArray,
      index + 2,
      notice[pathName][pathArray[index + 1]],
      fieldValue
    );
  } else if (pathArray[index + 1]) {
    if (!notice[pathName]) {
      notice[pathName] = {};
    }
    editNoticeValue(pathArray, index + 1, notice[pathName], fieldValue);
  } else {
    notice[pathName] = fieldValue;
  }
}

/**
 * Prépare l'objet issu de la contribution d'une notice, à être sauvegardé.
 * @param {Object} notice modifiée à sauvegarder
 * @param {{title: String, uuid: String}} database base dans laquelle on modifie
 * @param {Object} schema le métamodèle de la notice
 */
export function prepareNoticeForSave(_notice, database, schema) {
  // Deep clone pour éviter une erreur vuex
  const notice = JSON.parse(JSON.stringify(_notice));
  cleanObject(notice);
  setMissingSourceBlocs(notice, schema);
  // Remplacé par sourceModifiedBlocsk de ContributionForm :
  // checkSourcesDatabases(notice, database);
  setDatabaseList(notice, database);
  return notice;
}

/**
 * Vide tous les champs vides d'un objet (valeurs vides, obets vides, tableaux vides)
 * @param {*} object
 */
function cleanObject(object) {
  for (const key in object) {
    if (
      object[key] === "" ||
      object[key] === null ||
      (typeof object[key] === "object" && Object.keys(object[key]).length === 0)
    ) {
      delete object[key];
    } else if (
      Object.prototype.toString.call(object[key]) === "[object Object]"
    ) {
      cleanObject(object[key]);
    } else if (Array.isArray(object[key])) {
      for (const subKey in object[key]) {
        cleanObject(object[key][subKey]);
      }
      object[key] = object[key].filter(function(el) {
        if (el) {
          if (Object.keys(el).length > 0) {
            return el;
          }
          if (object[key].length === 0) {
            delete object[key];
          }
        } else {
          // TODO: que faire dans ce cas ?
        }
      });
    }
    if (
      (typeof object[key] === "object" &&
        Object.keys(object[key]).length === 0) ||
      (Array.isArray(object[key]) && object[key].length === 0)
    ) {
      delete object[key];
    }
  }
}

/**
 * Uniformise une chaine de caractères
 * @param {string} rawString
 */
export function stringNormalization(rawString) {
  rawString = rawString.toUpperCase();
  rawString = rawString.normalize("NFD").replace(/[\u0300-\u036F]/g, "");
  return rawString;
}

/**
 * Récupérer récursivement les providers (vee-validate) de tous les sous-composants;
 * à partir d'un composant d'entrée et d'une liste de sortie.
 * @param {VueComponent} component duquel récupérer les providers
 * @param {Array<VueComponent>} outProviders la liste de tous les providers en sortie
 */
export function getProviders(component, outProviders) {
  component.$children.forEach(child => {
    const provider = child.$refs.provider;
    const providerIsNotArray = !Array.isArray(provider);
    if (provider && providerIsNotArray) {
      outProviders.push(provider);
    }
    getProviders(child, outProviders);
  });
}

/**
 * Effectue une validation ASYNCHRONE d'un composant et de ses enfants.
 * Renvoie la liste des erreurs trouvées par les providers (vee-validate).
 * @param {VueComponent} component le composant (et ses enfants) à valider
 */
export async function validateForm(component) {
  const providers = [];
  getProviders(component, providers);
  const results = await Promise.all(providers.map(p => p.validate()));
  const errors = results.filter(result => !result.valid);
  return errors;
}

/**
 * Vérifie l'unicité de la notice enregistrée
 * @param {Object} listFields liste des champs de la notice
 * @param {String} noticeType type de notice sur laquelle faire la recherche
 * @param {Object} axios
 */
export async function checkUnicity(listFields, noticeType, axios) {
  let filters = [];
  for (const i in listFields) {
    if (listFields[i].required && listFields[i].value !== "") {
      filters = filters.concat(prepareFilter(i, listFields[i]));
    }
  }

  if (filters.size == 0) {
    return { count: 0 };
  }
  const searchParameters = {
    noticeType,
    contentTarget: "NOTICE",
    filters
  };

  let urlSearch = "";
  // TODO: ?
  // EncodeURI necessaire pour prévenir d'éventuels caractères spéciaux issus de la base 64 (ex: ">" => +)

  const buffer = Buffer.from(JSON.stringify(searchParameters));
  const base64 = buffer.toString("base64");
  urlSearch = "filters=" + base64;

  urlSearch = "/notice/advSearch?" + urlSearch;

  return await axios
    .get(urlSearch, { withCredentials: true })
    .then(response => {
      return response.data;
    })
    .catch(e => {
      return e.message;
    });
}

/**
 * Prépare le filtre pour la recherche avancée
 * @param {String} field champ à filtrer
 * @param {Object} properties propriétés du champ
 */
function prepareFilter(field, properties) {
  const selectedFields = [];
  const values = [];

  /// ## Process Field ##
  field = field.replace(/\[\]/g, "");
  field = field.replace(/(\.)([0-9]+)(\.)/g, ".");

  if (properties.type === "DataDate") {
    if (properties.value.start && properties.value.start.earliest) {
      values.push([
        {
          min: properties.value.start.earliest.date,
          max: properties.value.start.earliest.date
        }
      ]);
      if (properties.value.start.latest) {
        values.push([
          {
            min: properties.value.start.latest.date,
            max: properties.value.start.latest.date
          }
        ]);
      }
    }
    if (properties.value.end && properties.value.end.earliest) {
      values.push([
        {
          min: properties.value.end.earliest.date,
          max: properties.value.end.earliest.date
        }
      ]);
      if (properties.value.end.latest) {
        values.push([
          {
            min: properties.value.end.latest.date,
            max: properties.value.end.latest.date
          }
        ]);
      }
    } else {
      // Siecles non pris en charge dans la recherche avancée
    }
  } else if (properties.type === "DataConcept") {
    if (Array.isArray(properties.value)) {
      for (const index in properties.value) {
        values.push([
          '"' +
            properties.value[index].ref.substring(
              properties.value[index].ref.lastIndexOf("/") + 1,
              properties.value[index].ref.length
            ) +
            '"'
        ]);
      }
    } else {
      values.push([
        '"' +
          properties.value.ref.substring(
            properties.value.ref.lastIndexOf("/") + 1,
            properties.value.ref.length
          ) +
          '"'
      ]);
    }
  } else {
    /// ## Process Texte riche ##
    const originalValue = properties.value;
    const span = document.createElement("span");
    span.innerHTML = properties.value;
    properties.value = span.textContent || span.textContent;

    if (originalValue === properties.value) {
      // On n'est pas dans le cas d'un texte riche, on fait une recherche exacte avec des guillemets
      values.push(['"' + properties.value + '"']);
    } else {
      // Pour ne pas ramener de bruit, on fait une recherche exacte,
      // mais ne ramènera pas de résultat, car les balises du texte riches ne sont pas présentes
      values.push(['"' + properties.value + '"']);
    }
  }
  // Références non prises en charge dans la recherche avancée

  values.forEach(value => {
    const selectedField = {
      op: "ANY",
      field: "content." + field,
      boost: 1,
      values: value
    };
    selectedFields.push(selectedField);
  });

  return selectedFields;
}

/**
 * Check si la base de données principale est insérée dans les blocs sourcing
 * @param {Object} notice la notice créée
 * @param {Object} database la base de données selectionnée pour créer la notice
 */
function checkSourcesDatabases(notice, database) {
  for (const key in notice) {
    if (key === "sourcing" && notice[key].length) {
      let isNoticeDatabase = false;
      for (const i in notice[key]) {
        if (
          notice[key][i].database &&
          notice[key][i].database.ref === database.uuid
        ) {
          isNoticeDatabase = true;
          break;
        }
      }
      if (!isNoticeDatabase) {
        notice[key][notice[key].length] = {
          database: { ref: database.uuid, value: database.title }
        };
      }
    } else if (key === "sourcing") {
      notice[key].push({
        database: { ref: database.uuid, value: database.title }
      });
    }
    if (typeof notice[key] === "object") {
      checkSourcesDatabases(notice[key], database);
    }
  }
}

/**
 * Ajoute un bloc sourcing à tous les blocs pour lequel il est manquant
 * @param {Object} notice la notice créée
 * @param {Object} schema le schema du metamodel
 */
function setMissingSourceBlocs(notice, schema) {
  for (const tab in notice.content) {
    for (const bloc in notice.content[tab]) {
      for (const item in notice.content[tab][bloc]) {
        if (
          Array.isArray(notice.content[tab][bloc]) &&
          notice.content[tab][bloc][item] &&
          // Attention : la propriété items peut ne pas exister lors d'une création à la volée
          schema.properties[tab].properties[bloc]?.items.properties.sourcing &&
          !notice.content[tab][bloc][item].sourcing
        ) {
          notice.content[tab][bloc][item].sourcing = [];
        }
      }
    }
  }
}

/**
 * Ajoute la database sur laquelle se base la notice à la liste des base de données liées à la notice
 * @param {Object} notice la notice créée
 * @param {Object} database la base de données
 */
function setDatabaseList(notice, database) {
  const newDatabase = { ref: database.uuid, value: database.title };
  if (!notice.content) {
    notice.content = {};
  }
  if (notice.content.recordManagementInformation) {
    if (notice.content.recordManagementInformation.databaseLabel) {
      notice.content.recordManagementInformation.databaseLabel.push(
        newDatabase
      );
    } else {
      notice.content.recordManagementInformation.databaseLabel = [newDatabase];
    }
  } else {
    notice.content.recordManagementInformation = {
      databaseLabel: [newDatabase]
    };
  }

  const actualList = notice.content.recordManagementInformation.databaseLabel;
  notice.content.recordManagementInformation.databaseLabel = actualList.filter(
    (a, b) =>
      actualList
        .map(function(e) {
          return e.ref;
        })
        .indexOf(a.ref) === b && a.ref
  );
}
