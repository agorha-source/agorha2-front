import atob from "atob";
import { decodeFacets, encodeFacets } from "@/plugins/facets";

/**
 * Liste des noeuds cochés dans les arbres passés en paramètre.
 * (i) Ne modifie pas le store vuex.
 * @param {*} trees this.$refs.general ou this.$refs.specific
 */
export function getCheckedNodesFromTrees(trees) {
  const checkedNodes = [];
  if (trees) {
    for (const tree of trees) {
      const checked = tree.checked();
      checked.forEach(node => checkedNodes.push(node));
    }
  }
  return checkedNodes;
}

/**
 * Transforme un objet aggregations en checkedNodes.
 * (i) Ne modifie pas le store vuex.
 * @param {Object} aggregations
 */
export function aggregationsToCheckedNodes(aggregations) {
  const nodes = [];
  // Fabrication de checkedNodes
  Object.keys(aggregations).forEach((aggKey) => {
    // On exclut les filtres sur les onglets
    if (aggKey !== "noticeType") {
      const filters = [...aggregations[aggKey].filters];
      const facetScope = aggregations[aggKey].facetScope;
      filters.forEach((filter) => {
        nodes.push({
          text: filter,
          facetKey: aggKey,
          key: filter,
          scope: facetScope,
          id: `from_url::${aggKey}::${filter}`
        });
      });
    }
  });
  return nodes;
}

/**
 * Fusionne les noeuds cochés dans une copie de l'objet aggregations
 * et renvoie un objet contenant uniquement les aggregations non vides
 * (dont le tableau filters n'est pas vide).
 * (i) NE doit PAS modifier le store vuex !
 * @param {Array} checkedNodes les noeuds cochés dans l'arbre des facettes
 * @param {Object} aggregations le futur objet des facettes
 * @param {Boolean} encoded pour les récupérer encodées
 */
export function getSignificantFacets(
  checkedNodes,
  aggregations,
  encoded = false
) {
  /**
   * ATTENTION ! On réalise un deep clone pour éviter de
   * conserver des références aux objets du store.
   * Sinon, on s'expose à une erreur de vuex qui refuse
   * les modifications d'objets du store hors de mutations.
   */
  const aggs = JSON.parse(JSON.stringify(aggregations));
  for (const node of checkedNodes) {
    let facetKey = "";
    let key = "";
    if (node.data) {
      facetKey = node.data.facetKey;
      key = node.data.key;
    } else {
      facetKey = node.facetKey;
      key = node.key;
    }
    if (aggs[facetKey]) {
      // Ajout du filtre de noeud aux facettes
      const filters = aggs[facetKey].filters ?? [];
      if (!filters.includes(key)) { filters.push(key); }
    }
  }
  const significant = {};
  Object.keys(aggs).forEach((key) => {
    if (aggs[key].filters.length) {
      significant[key] = aggs[key];
    }
  });
  if (encoded) {
    return encode(significant);
  } else {
    return significant;
  }
}

/**
 * Descente dans le schéma pour trouver les facettes qui ne sont pas au 1er niveau.
 * Attention : modifie l'objet en entrée (list) !
 * @param {object} list liste des facettes existantes
 * @param {object} schemaBloc schema du bloc parcouru
 * @param {string} path chemin du bloc (sans les "properties" et "items")
 * @param {boolean} isSpecific true si le type de facette recherché est spécifié, false
 * sinon (ne retourne alors que les facettes générales)
 */
function childrenFacets(list, blockSchema, path, specific) {
  if (blockSchema) {
    for (const name in blockSchema) {
      if (
        blockSchema[name] &&
        blockSchema[name].facetScope &&
        (blockSchema[name]?.facetScope === "general" ||
          (specific && blockSchema[name]?.facetScope !== "general"))
      ) {
        const facet = initFacet(blockSchema[name], name, path);
        list[blockSchema[name].facetDisplayName] = facet;
      } else if (blockSchema[name] && typeof blockSchema[name] === "object") {
        let newPath = path;
        if (name !== "properties" && name !== "items") {
          newPath += "." + name;
        }
        childrenFacets(list, blockSchema[name], newPath, specific);
      }
    }
  }
}

/**
 * Construit et retourne l'objet 'aggregations' pour requête au moteur de recherche.
 * Cet objet permet de filtrer les résultats de recherche.
 * @param {Object} schema le métamodèle listant les filtres possibles
 * @param {Boolean} specific true si le type de facette recherché est spécifié, false
 *  sinon (ne retourne alors que les facettes générales)
 * @param {String} selectedTab utile pour les facettes des news
 */
export function computeAggregations(schema, specific, selectedTab) {
  const aggregations = {};
  const internalProps = schema.properties.internal.properties;
  const contentProps = schema.properties.content.properties;
  // Facettes des onglets
  const ntFacet = initFacet(internalProps.noticeType, "noticeType", "internal");
  aggregations.noticeType = ntFacet;
  // Facettes statiques sur status
  const stFacet = initFacet(internalProps.status, "status", "internal");
  aggregations[internalProps.status.facetDisplayName] = stFacet;
  // Récupération des facettes
  if (selectedTab !== "NEWS") {
    for (const bloc in contentProps) {
      if (
        contentProps[bloc]?.facetScope &&
        contentProps[bloc]?.facetScope === "general"
      ) {
        const facet = initFacet(contentProps[bloc], bloc, "content");
        aggregations[contentProps[bloc].facetDisplayName] = facet;
      } else if (
        contentProps[bloc]?.facetScope &&
        contentProps[bloc]?.facetScope !== "general" &&
        specific
      ) {
        const facet = initFacet(contentProps[bloc], bloc, "content");
        aggregations[contentProps[bloc].facetDisplayName] = facet;
      } else {
        childrenFacets(
          aggregations,
          contentProps[bloc],
          "content." + bloc,
          specific
        );
      }
    }
  } else {
    aggregations["Catégories"] = initFacet({}, "tags", "content", false);
    aggregations["Bases de données"] = initFacet({}, "databases", "internal.digest", false);
    aggregations.Types = initFacet({}, "newsType", "internal.digest", false);
    aggregations["Date de création"] = initFacet({}, "createdDatePath", "internal.digest", true);
  }
  return aggregations;
}

export const encode = facets => encodeFacets(facets);
export const decode = facets => decodeFacets(facets);

/**
 * Initialise l'objet facette adapté.
 * @param {object} schema le schéma du bloc contenant la facette
 * @param {string} blocName le nom du bloc courant
 * @param {string} path chemin du bloc (sans les "properties" et "items")
 * @param {boolean} pathHierarchical permet de renseigner si une facette est hiérarchique - utile pour les facettes des news, construites sans schema
 */
function initFacet(schema, blocName, path, pathHierarchical = false) {
  if ((schema?.facetType && schema.facetType === "hierarchical") || pathHierarchical) {
    pathHierarchical = true;
  }
  return {
    filters: [],
    field: path + "." + blocName,
    facetScope: schema?.facetScope ?? "",
    facetOrder: schema?.facetOrder ?? 0,
    pathHierarchical
  };
}

/**
 * Décode une chaîne de caractères donnée en base64.
 * @param {string} str la chaîne de caractères encodée en base64
 */
export function b64DecodeUnicode(str) {
  // From bytestream, to percent-encoding, to original string.
  return decodeURIComponent(
    atob(str)
      .split("")
      .map(function(c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );
}
