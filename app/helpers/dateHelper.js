import { format, formatDistanceToNow } from "date-fns";
import { fr } from "date-fns/locale";

/**
 * Formate la date renseignée au format "DD/MM/YYYY hh:mm".
 * (!) Pour date-fns le bon format est : "dd/MM/yyyy hh:mm".
 * @param {String} date la date à formater
 */
export function _formatDate(date, targetFormat = "dd/MM/yyyy HH:mm") {
  try {
    if (!date) { return ""; }
    const formatted = format(new Date(date), targetFormat);
    return String(formatted);
  } catch (error) {
    console.error("catch !");
    console.error(error);
    return "[ERREUR] Date invalide";
  }
}

/**
 * L'écart entre la date et maintenant en français.
 * @param {String} date la date à comparer
 */
export function _fromNow(date) {
  return formatDistanceToNow(
    new Date(date),
    { locale: fr, includeSeconds: true, addSuffix: true }
  );
}
