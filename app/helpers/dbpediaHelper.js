/**
 * Ce module est un fork de dbpedia-entity-lookup et le remplace dans ce projet.
 * https://github.com/cwrc/dbpedia-entity-lookup
 *
 * Il corrige les appels incorrects à lookup.dbpedia.org.
 * Seules les méthodes getEntitySourceURI et callDBPedia (mapResponse) ont été modifiées.
 * (i) Les résultats ayant un score > 1000 uniquement (pertinents) sont renvoyés.
 */

"use strict";

/*
      config is passed through to fetch, so could include things like:
      {
          method: 'get',
          credentials: 'same-origin'
    }
    Note that the default config includes the accept header.  If an over-riding config
    is passed in, don't forget to set the accept header so we get json back from dbpedia
    and not XML.
*/

const fetchWithTimeout = (url, config = { headers: { Accept: "application/json" } }, time = 30000) => {
  /*
        the reject on the promise in the timeout callback won't have any effect, *unless*
        the timeout is triggered before the fetch resolves, in which case the setTimeout rejects
        the whole outer Promise, and the promise from the fetch is dropped entirely.
    */

  // Create a promise that rejects in <time> milliseconds
  const timeout = new Promise((resolve, reject) => {
    const id = setTimeout(() => {
      clearTimeout(id);
      reject("Call to DBPedia timed out");
    }, time);
  });

  // Returns a race between our timeout and the passed in promise
  return Promise.race([fetch(url, config), timeout]);
};

// note that this method is exposed on the npm module to simplify testing,
// i.e., to allow intercepting the HTTP call during testing.
const getEntitySourceURI = (queryString, queryClass) => {
  const SEARCH_ENDPOINT = "https://lookup.dbpedia.org/api/search";
  // Calls directly http://lookup.dbpedia.org
  // FIXES https://github.com/cwrc/dbpedia-entity-lookup/issues/13
  return `${SEARCH_ENDPOINT}/KeywordSearch?QueryClass=${queryClass}&MaxHits=5&QueryString=${encodeURIComponent(queryString)}&format=json_raw`;
  // Caution: adding query param &format=json_raw since header is ignored (Accept: "application/json")
};

const getPersonLookupURI = queryString => getEntitySourceURI(queryString, "person");
const getPlaceLookupURI = queryString => getEntitySourceURI(queryString, "place");
const getOrganizationLookupURI = queryString => getEntitySourceURI(queryString, "organisation");
const getTitleLookupURI = queryString => getEntitySourceURI(queryString, "work");
const getRSLookupURI = queryString => getEntitySourceURI(queryString, "thing");

const callDBPedia = async(url, queryString, queryClass) => {
  const response = await fetchWithTimeout(url)
    .catch((error) => {
      return error;
    });

  // if status not ok, through an error
  if (!response.ok) {
    throw new Error(
      `Something wrong with the call to DBPedia, possibly a problem with the network or the server. HTTP error: ${response.status}`
    );
  }

  const responseJson = await response.json();

  const mapResponse = responseJson.docs.map(({
    score,
    resource,
    label,
    comment = "No description available"
  }) => {
    const _resource = resource.shift() ?? "";
    const _label = label.shift() ?? "";
    const _comment = comment.shift() ?? "";
    const _score = score.shift() ?? 0;
    return {
      nameType: queryClass,
      id: _resource,
      uri: _resource,
      uriForDisplay: _resource,
      name: _label,
      repository: "dbpedia",
      originalQueryString: queryString,
      description: _comment,
      score: _score
    };
  }
  // Uniquement les résultats pertinents
  ).filter(x => x.score > 1000);

  return mapResponse;
};

const findPerson = queryString => callDBPedia(getPersonLookupURI(queryString), queryString, "person");
const findPlace = queryString => callDBPedia(getPlaceLookupURI(queryString), queryString, "place");
const findOrganization = queryString => callDBPedia(getOrganizationLookupURI(queryString), queryString, "organisation");
const findTitle = queryString => callDBPedia(getTitleLookupURI(queryString), queryString, "work");
const findRS = queryString => callDBPedia(getRSLookupURI(queryString), queryString, "thing");

export default {
  findPerson,
  findPlace,
  findOrganization,
  findTitle,
  findRS,
  getPersonLookupURI,
  getPlaceLookupURI,
  getOrganizationLookupURI,
  getTitleLookupURI,
  getRSLookupURI
};
