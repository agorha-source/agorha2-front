/** Indique si une facette représentée par {key, id} se trouve dans les checkedNodes */
function isChecked(key, id, checkedNodes) {
  const checked = checkedNodes.some((x) => {
    return x.key === key && x.facetKey === id; // Note: la catégorie d'une facette est 'id'
  });
  return checked;
}

/**
 * Récupère les facettes et les formate au format LiquorTree
 * pour affichage sur la page de résultats de recherche.
 * @param {*} facets les facettes de la réponse de recherche
 */
export function computeFacets(facets, checkedNodes) {
  const computed = [];
  facets.forEach((item) => {
    const { entries, facetScope, id, pathHierarchical } = item;
    if (facetScope !== "tab" && entries) {
      const subRes = [];
      entries.forEach((entry) => {
        const key = entry.value;
        const checked = isChecked(key, id, checkedNodes);
        const facet = {
          text: getFacetKey(key, entry.count, pathHierarchical), // FIXME: changer le traitement ?
          data: { facetKey: id, key, count: entry.count },
          state: { checked }
        };
        // checkSelectedFacets(facet, id, key);
        if (entry.subEntries) {
          facet.children = getChildren(id, entry.subEntries, checkedNodes);
        }
        subRes.push(facet);
      });
      computed.push({ name: id, facetScope, entries: subRes });
    }
  });
  return computed;
}

/**
 * Récupère les facettes et les formate au format LiquorTree
 * pour affichage sur la page de résultats de recherche.
 * @param {*} facets les facettes de la réponse de recherche
 * @param checkedNodes
 */
export function computeMediaFacets(facets, checkedNodes) {
  const computed = [];
  facets.forEach((item) => {
    const { entries, id, pathHierarchical } = item;
    if (entries) {
      const subRes = [];
      entries.forEach((entry) => {
        const key = entry.value;
        const checked = checkedNodes.some((x) => {
          return x.key === key && x.facetKey === id; // Note: la catégorie d'une facette est 'id'
        });
        const facet = {
          text: getFacetKey(key, entry.count, pathHierarchical), // FIXME: changer le traitement ?
          data: { facetKey: id, key, count: entry.count },
          state: { checked }
        };
        // checkSelectedFacets(facet, id, key);
        if (entry.subEntries) {
          facet.children = getChildren(id, entry.subEntries);
        }
        subRes.push(facet);
      });
      computed.push({ name: id, facetScope: "general", entries: subRes });
    }
  });
  return computed;
}

/**
 * @deprecated
 * TODO: supprimer si n'a aucun effet
 * Vérifie les filtres de facettes existants et coche les facettes
 * déjà sélectionnées et ouvre la branche
 * @param {Array} facets les facettes courantes
 * @param {Object} facet la facette à vérifier
 * @param {String} facetName le nom de la facette
 * @param {String} key la valeur de la facette
 */
function checkSelectedFacets(facets, facet, facetName, key) {
  let isParentOpen = false;
  const facetWithState = { ...facet, state: { checked: false } };
  for (const filterIndex in facets) {
    const filterData = facets[filterIndex].data;
    if (filterData) {
      if (filterData?.facetKey === facetName && filterData?.key === key) {
        facetWithState.state.checked = true;
        isParentOpen = true;
      }
    }
  }
  return isParentOpen;
}

/**
 * Récupère les sous catégories des facettes pour les afficher dans l'arbre.
 * @param {string} facetName le nom de la facette parent
 * @param {array} node la liste des sous catégories
 * @param {Array} checkedNodes la liste des noeuds à cocher dans l'arbre des facettes (issue de l'URL)
 */
function getChildren(facetName, node, checkedNodes = []) {
  const res = [];
  node?.forEach((item) => {
    const key = item.value;
    const child = {
      text: getFacetKey(key, item.count, true),
      data: {
        facetKey: facetName,
        key,
        count: item.count,
        facetScope: item.facetScope
      }
    };

    const checked = isChecked(key, facetName, checkedNodes);
    if (checked) {
      child.state = { checked };
    }
    // checkSelectedFacets(child, facetName, key);
    if (item.subEntries) {
      child.children = getChildren(facetName, item.subEntries, checkedNodes);
    }
    res.push(child);
  });
  return res;
}

function getFacetKey(key, count, pathHierarchical) {
  return pathHierarchical
    ? key.substr(key.lastIndexOf("/") + 1, key.length) + ` (${count})`
    : `${key} (${count})`;
}
