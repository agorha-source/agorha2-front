/**
 * TODO: refactor
 * Renvoie les filtres au format accepté par l'API de recherche avancée.
 * @param {*} resultsFilters
 */
export function frontFilterToBackFilter(resultsFilters) {
  const filters = [];
  const storeFilters = resultsFilters;
  // Initialisation des filtres
  for (const i in storeFilters) {
    const filterValues = [];
    if (
      storeFilters[i].type === "DataSimple" ||
      storeFilters[i].type === "DataConcept" ||
      storeFilters[i].type === "DataReference" ||
      storeFilters[i].type === "DataText"
    ) {
      for (const j in storeFilters[i].values) {
        filterValues.push(storeFilters[i].values[j].value);
      }
    } else {
      for (const j in storeFilters[i].values) {
        let val = {};
        if (storeFilters[i].values[j].opEqu === "[]") {
          val = {
            min: formatBCDate(storeFilters[i].values[j].date1),
            max: formatBCDate(storeFilters[i].values[j].date2)
          };
        } else {
          let value;
          if (storeFilters[i].type === "DataDate") {
            value = formatBCDate(storeFilters[i].values[j].value);
          } else {
            value = storeFilters[i].values[j].value;
          }
          if (storeFilters[i].values[j].opEqu === "=") {
            val = {
              min: value,
              max: value
            };
          } else if (storeFilters[i].values[j].opEqu === ">") {
            val = {
              min: value,
              max: null
            };
          } else if (storeFilters[i].values[j].opEqu === "<") {
            val = {
              min: null,
              max: value
            };
          }
        }
        filterValues.push(val);
      }
    }
    const newFilter = {
      field: storeFilters[i].id,
      boost: storeFilters[i].boost,
      op: storeFilters[i].opBool,
      values: filterValues
    };
    filters.push(newFilter);
  }
  return filters;
}

/**
 * Met au format back les dates avant JC
 * @param {string} date la date à formater
 **/
function formatBCDate(date) {
  const dateArray = date.split("/");
  let dateBC = "";
  if (dateArray[2].includes("-")) {
    dateBC = dateArray[2].substr(1, dateArray[2].length);
    dateBC += "-" + dateArray[1] + "-" + dateArray[0];
    dateBC += " BC";
  } else {
    dateBC = dateArray[2];
    dateBC += "-" + dateArray[1] + "-" + dateArray[0];
    dateBC += " AD";
  }
  return dateBC;
}
