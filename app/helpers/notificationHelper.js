// TODO: refactor en mixin pour avoir accès à this.$bvToast

/**
 * Permet d'afficher un toast qui disparaîtra au bout de {autoHideDelay} secondes
 * @param bvtoast
 * @param content message que l'on veut afficher
 * @param title titre du toast, peut ne pas être renseigné
 * @param autoHideDelay 5000 = 5secondes
 * @param variant Default Primary Secondary Danger Warning Success Info
 */
export function createToastwithAutoHideDelay(
  bvtoast,
  content,
  title,
  autoHideDelay,
  variant
) {
  bvtoast.toast(content, {
    title,
    autoHideDelay,
    appendToast: false,
    variant
  });
}

/**
 * Permet d'afficher un toast
 * @param bvtoast
 * @param content message que l'on veut afficher
 * @param title titre du toast, peut ne pas être renseigné
 * @param variant Default Primary Secondary Danger Warning Success Info
 */
export function createToast(bvtoast, content, title, variant) {
  bvtoast.toast(content, {
    title,
    appendToast: false,
    variant,
    solid: true
  });
}

/**
 * Permet d'afficher un toast dont le message est un lien
 * @param bvtoast
 * @param link
 * @param content message que l'on veut afficher
 * @param title titre du toast, peut ne pas être renseigné
 * @param variant Default Primary Secondary Danger Warning Success Info
 */
export function createToastWithLink(bvtoast, link, content, title, variant) {
  bvtoast.toast(content, {
    href: link,
    title,
    autoHideDelay: 5000000,
    appendToast: false,
    variant
  });
}
