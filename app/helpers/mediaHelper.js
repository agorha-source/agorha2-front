/**
 * @private
 * @returns []
 */
export function getMediaFacetsEncoded(params = null) {
  const facets = {};
  const noticesLinked = initMediaFacet("internal", "linkedWithNotices", false);
  const contentType = initMediaFacet("files", "computedFacetMediaType", true);
  const database = initMediaFacet("database", "value", false);
  facets["Notices liées"] = noticesLinked;
  facets["Types de contenu"] = contentType;
  facets["Bases de données"] = database;

  // Ajout des filtres courants
  for (const facet in facets) {
    for (const i in params) {
      if (facet === params[i]) {
        facets[facet].filters.push(params[i].filters);
      }
    }
  }
  return facets;
}

export function initMediaFacet(path, blocName, pathHierarchical) {
  return {
    filters: [],
    field: path + "." + blocName,
    facetOrder: 0,
    pathHierarchical
  };
}

/**
 * Récupère les informations
 */
export function getMediaToUpdate(axios, mediaId) {
  // Récupération des infos du média
  const url = "/media/" + mediaId;
  return axios
    .get(url)
    .then((response) => {
      return response.data;
    })
    .catch();
}
