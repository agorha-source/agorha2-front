// Ce fichier de configuration décrit les thésaurus externes consultables pour chaque catégorie de thésaurus
// /!\ Merci de saisir les valeurs en minuscules /!\

export const ExternalThesaurus = {
  place: ["wikidata", "dbpedia", "geonames", "getty"],
  title: ["wikidata", "dbpedia"],
  person: ["wikidata", "dbpedia", "getty"],
  organization: ["wikidata", "dbpedia"]
};
