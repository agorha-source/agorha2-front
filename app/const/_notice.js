export const statusEnum = {
  CREATION_IN_PROGRESS: "En cours",
  TO_PUBLISH: "A publier",
  PUBLISHED: "Publiée",
  ARCHIVED: "Archivée"
};

export const typesEnum = {
  null: "Tous les types",
  ARTWORK: "Œuvre",
  PERSON: "Personne",
  EVENT: "Evénement",
  REFERENCE: "Référence",
  COLLECTION: "Collection",
  NEWS: "Article ou actualité"
};

export const tabsEnum = {
  ALL: "Tous les résultats",
  ARTWORK: "Œuvres",
  PERSON: "Personnes",
  REFERENCE: "Références",
  EVENT: "Evénements",
  COLLECTION: "Collections",
  NEWS: "Articles & actualités"
};

export const linkedNoticesEnum = {
  ARTWORK: "Voir toutes les oeuvres",
  PERSON: "Voir toutes les personnes",
  REFERENCE: "Voir toutes les références",
  EVENT: "Voir tous les événements",
  COLLECTION: "Voir toutes les collections",
  NEWS: "Voir tous les articles et actualités"
};

// Format du tableau des options de b-form-select
export const NOTICE_TYPES = Object.keys(typesEnum).map(key =>
  Object({ value: key, text: typesEnum[key] })
);

export const associationLieuxEnum = {
  ARTWORK: [
    "Lieu de conservation",
    "Lieu de découverte",
    "Lieu de création"
  ],
  COLLECTION: "Lieu de collection",
  REFERENCE: "Lieu d'édition",
  PERSON: ["Lieu de naissance", "Lieu de mort", "Lieu d'activité"],
  EVENT: "Lieu"
};

export const associationCouleurEnum = {
  ARTWORK: "#72b8ae",
  COLLECTION: "#495057",
  REFERENCE: "yellow",
  PERSON: "blue",
  EVENT: "red"
};
