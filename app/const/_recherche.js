/** Types d'affichage des résultats de recherche */
export const DISPLAY_TYPE = Object.freeze({
  cartouche: "cartouche",
  mosaique: "mosaique",
  liste: "liste"
});

/** Types de recherche */
export const SEARCH_TYPE = Object.freeze({
  simple: "simple",
  advanced: "advanced"
});

/** Champ sur lequel le tri est appliqué  */
export const FEILD_SORT = Object.freeze({
  TITLE: "TITLE", // associé à l'ordre ASC
  UPDATED_DATE: "UPDATED_DATE" // associé à l'ordre DESC
});

/** Types de tri */
export const SORT_ORDER = Object.freeze({
  ASC: "ASC",
  DESC: "DESC"
});

const pageSizeEnum = {
  20: "20 résultats par page",
  50: "50 résultats par page",
  100: "100 résultats par page",
  250: "250 résultats par page",
  500: "500 résultats par page",
  1000: "1000 résultats par page"
};

const noticeNumberEnum = {
  20: "20 notices",
  50: "50 notices",
  100: "100 notices",
  250: "250 notices",
  500: "500 notices",
  1000: "1000 notices"
};

const fieldSortEnum = {
  "": "Tri par pertinence",
  UPDATED_DATE: "Tri par ordre chronologique",
  TITLE: "Tri par ordre alphabétique"
};

const mediaFieldSortEnum = {
  PERTINENCE: "Tri par pertinence",
  UPDATED_DATE: "Tri par ordre chronologique",
  TITLE: "Tri par ordre alphabétique"
};

export const SELECTION_MESSAGE =
  "Vous devez d’abord sélectionner des résultats dans la recherche pour utiliser cette fonctionnalité.";

// Format du tableau des options de b-form-select
export const pageSizeOptions = Object.keys(pageSizeEnum).map(key =>
  Object({ value: key, text: pageSizeEnum[key] })
);
export const noticeNumberOptions = Object.keys(noticeNumberEnum).map(key =>
  Object({ value: key, text: noticeNumberEnum[key] })
);
// Format du tableau des options de b-form-select
export const fieldSortOptions = Object.keys(fieldSortEnum).map(key =>
  Object({ value: key, text: fieldSortEnum[key] })
);
// Format du tableau des options de b-form-select pour les médias
export const mediaFieldSortOptions = Object.keys(mediaFieldSortEnum).map(key =>
  Object({ value: key, text: mediaFieldSortEnum[key] })
);
