module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: "babel-eslint"
  },
  extends: ["@nuxtjs", "plugin:nuxt/recommended"],
  plugins: [],
  // Règles custom
  rules: {
    "vue/html-self-closing": ["error", { html: { void: "always" } }],
    "space-before-function-paren": ["error", "never"],
    "vue/singleline-html-element-content-newline": ["off"],
    // Point-virgule pour chaque instruction
    semi: ["error", "always"],
    // Guillemets presque partout
    quotes: ["error", "double", { allowTemplateLiterals: true }],
    // Pas de console log, on autorise warn et error
    "no-console": ["error", { allow: ["warn", "error"] }]
  }
};
