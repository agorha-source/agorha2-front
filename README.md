# agorha2-front

Front (vue.js)

# reconstruction de l'image du container:

```
cd app
docker build -t agorha2/front:1.11.2.2 .
docker save agorha2/front:1.11.2.2 -o agorha2-front.1.11.2.2.tar
```


sur le serveur de production

```
docker load -i agorha2-front.1.11.2.2.tar
vim /opt/app/node/docker/docker-compose.yaml
```


changer la version dans le fichier docker-compose.yaml dans l'attribut "image:".

```
cd /opt/app/node/docker
docker compose down
docker compose up -d
```

## information sur le docker-compose.yaml

Le docker-compose.yaml contient la directive 'NPM_TARGET', elle pointe vers une valeur définie dans le package.json.
avec la valeur 'start-prod', c'est le fichier de configuration app/env/prod.json qui sera chargé.

```yaml
services:
  nuxt01:
    restart: always
    image: agorha2/front:1.11.2.2
    environment:
      NPM_TARGET: start-prod
    ports:
      - 3000:5000
  nuxt02:
    restart: always
    image: agorha2/front:1.11.2.2
    environment:
      NPM_TARGET: start-prod
    ports:
      - 3001:5000
  nuxt03:
    restart: always
    image: agorha2/front:1.11.2.2
    environment:
      NPM_TARGET: start-prod
    ports:
      - 3002:5000
  nuxt04:
    restart: always
    image: agorha2/front:1.11.2.2
    environment:
      NPM_TARGET: start-prod
    ports:
      - 3003:5000
```

